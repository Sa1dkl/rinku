export const environment = {
  production: false,
  hmr: true,
  inHouse: [true, true],
  configFile: 'assets/settings.json',
  SSO: 'http://devsso.coppel.com/api',
  STATIC: {
    webbridge: 'http://localhost:20542/api/huella',
    hojaAzul: 'http://10.44.15.147/hojaazul/fotos/',
    redirect: 'http://intranet.cln/intranet',
  },
  haveMenu: true,
  appLogo: 'assets/img/app-logo.png',
};
