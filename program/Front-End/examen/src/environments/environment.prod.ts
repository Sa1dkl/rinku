export const environment = {
  production: false,
  hmr: false,
  inHouse: [false, false],
  configFile: 'assets/settings.prod.json',
  SSO: 'http://sso.coppel.io:50060/api',
  apiRinku: 'http://10.28.114.59/examen/',
  //apiRinku: 'http://localhost/examen/',
  STATIC: {
    webbridge: 'http://localhost:20542/api/huella',
    hojaAzul: 'http://10.44.15.147/hojaazul/fotos/',
    redirect: 'http://http://localhost:4200/#/examen',
  },
  haveMenu: true,
  appLogo: 'assets/img/app-logo.png',
};
