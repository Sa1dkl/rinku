import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneranominaService {

  constructor(private http: HttpClient) { }

  public generarnominas(fecha) {
    return this.http.get(environment.apiRinku + "api/rinku/generar/nominas?"
      + "fecha=" + fecha);
  }

  public consultarnominas() {
    return this.http.get(environment.apiRinku + "api/rinku/nominas");
  }

  public consultarempleadosfiltro() {
    return this.http.get(environment.apiRinku + "api/rinku/empleados/filtros");
  }

  public consultarflagentregas(fecha) {
    return this.http.get(environment.apiRinku + "api/rinku/entregas/flag?"
      + "fecha=" + fecha);
  }

  public generarpdf() {
    return this.http.get(environment.apiRinku + "api/rinku/nomina/pdf");
  }
}