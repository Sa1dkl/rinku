
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { json } from 'd3';

import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { MessageService } from 'primeng/components/common/messageservice';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ConfigService } from './config.service';

@Injectable()
export class AuthenticationService {
  @BlockUI() blockUI: NgBlockUI;
  public settings: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private toasterService: ToasterService,
    private message: MessageService,
    private configService: ConfigService
  ) {
    this.settings = configService.getConfig();
  }

  login(_username: string, _password: string) {
    return this.http.post<any>('/api/authenticate', { username: _username, password: _password }).pipe(map((user) => {
      // login successful if there's a jwt token in the response
      if (user && user.token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        sessionStorage.setItem('currentUser', JSON.stringify(user));
      }
      return user;
    }));
  }

  loginHuella(credentials: any) {
    this.http.post(environment.SSO + '/v1/login', credentials).subscribe((respuesta: any) => {
      this.setLogin(respuesta.data.empleado, respuesta.data.token);
    }, (error) => {
      const toast: Toast = {
        type: 'error',
        title: 'Mensaje',
        body: error.error.meta.error.userMessage,
      };
      this.toasterService.pop(toast);
    });
  }

  loginAutomatico(_token: string) {
    this.http.post<any>(environment.SSO + '/v1/verify', { token: _token }).subscribe(
      (respuesta) => {
        this.http.post<any>(environment.SSO + '/v1/me', { token: _token }).subscribe(
          (resp) => {
            const info = resp.data;
            info.token = _token;
            this.setLogin(info, _token);
          },
          (error) => {
          }
        );
      },
      (err) => {
        this.blockUI.stop();
        const toast: Toast = {
          type: 'error',
          title: 'Mensaje',
          body: err.error.meta.error.userMessage,
        };
        this.toasterService.pop(toast);
      }
    );
  }

  isAuth() {
    if (sessionStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }

  currentUser() {
    return JSON.parse(sessionStorage.getItem('currentUser'));
  }

  logout() {
    this.deletesessionStorage();
    if (environment.inHouse[0]) {
      this.router.navigate(['/login']);
    } else {
      window.location.href = environment.STATIC.redirect;
    }
  }

  setLogin(user: any, token: any) {
    sessionStorage.setItem('currentUser', JSON.stringify(user));
    sessionStorage.setItem('token', token);
    this.blockUI.stop();
    this.router.navigate(['/']);
  }

  private deletesessionStorage() {
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem('token');
  }
}
