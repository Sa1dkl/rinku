import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  constructor(private http: HttpClient) { }

  public actualizarempleados($jsonEmpleados) {
    return this.http.put(environment.apiRinku + "api/rinku/empleados?",
      $jsonEmpleados
    );
  }

  public consultaempleados() {
    return this.http.get(environment.apiRinku + "api/rinku/empleados");
  }

  public altaempleados($jsonEmpleados) {
    return this.http.post(environment.apiRinku + "api/rinku/empleados",
      $jsonEmpleados
    );
  }

  public consultanumEmpelado() {
    return this.http.get(environment.apiRinku + "api/rinku/numempleados");
  }

  public consultarcorreos(correo) {
    return this.http.get(environment.apiRinku + "api/rinku/correos?"
      + "correo=" + correo);
  }
}