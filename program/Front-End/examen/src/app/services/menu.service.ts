import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class MenuService {
  public apiRoot: string;
  public menuExist = new BehaviorSubject(false);

  navItem$ = this.menuExist.asObservable();

  constructor(public http: HttpClient, private config: ConfigService) {
    const cnfg = config.getConfig();

    this.apiRoot = './assets/menu.json';
    if (cnfg.apiMenu) {
      this.apiRoot = cnfg.apiMenu;
    }
  }

  getMenu(): Observable<any> {
    return this.http.get(this.apiRoot);
  }

  setExistMenu(value: boolean) {
    this.menuExist.next(value);
  }
}
