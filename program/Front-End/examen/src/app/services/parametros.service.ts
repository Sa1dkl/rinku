import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  public obtenerUsuario(): string {
    let user = JSON.parse(sessionStorage.currentUser);
    return user.numeroempleado;
  }

  private getUserToken(): string {
    return sessionStorage.token;
  }

  private currentUser() {
    return JSON.parse(sessionStorage.getItem('currentUser'));
  }

  constructor(private http: HttpClient) { }

  public consultaroles() {
    return this.http.get(environment.apiRinku + "api/rinku/roles");
  }

  public consultabonos() {
    return this.http.get(environment.apiRinku + "api/rinku/bonos");
  }

  public consultasueldos(idRoll) {
    return this.http.get(environment.apiRinku + "api/rinku/sueldos?"
      + "idRoll=" + idRoll);
  }

  public consultasparametrosbonos(idBono) {
    return this.http.get(environment.apiRinku + "api/rinku/parametros/bonos?"
      + "idBono=" + idBono);
  }

  public consultarisr(validar) {
    return this.http.get(environment.apiRinku + "api/rinku/isrs?"
      + "validar=" + validar);
  }

  public actualizarbonos($jsonBonos) {
    return this.http.put(environment.apiRinku + "api/rinku/bonos",
      $jsonBonos
    );
  }

  public actualizarsueldos($jsonSuedos) {
    return this.http.put(environment.apiRinku + "api/rinku/sueldos",
      $jsonSuedos
    );
  }

  public actualizarisr($jsonIsr) {
    return this.http.put(environment.apiRinku + "api/rinku/isrs",
      $jsonIsr
    );
  }

  public consultanumEmpelado() {
    return this.http.get(environment.apiRinku + "api/rinku/numempleados");
  }
}