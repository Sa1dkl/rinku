import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntregasService {


  constructor(private http: HttpClient) { }

  public consultarempleadosfiltro() {
    return this.http.get(environment.apiRinku + "api/rinku/empleados/filtros");
  }

  public consultarempleadosfiltrocubrepuestos() {
    return this.http.get(environment.apiRinku + "api/rinku/empleados/filtros/puestos");
  }

  public consultanumEmpelado() {
    return this.http.get(environment.apiRinku + "api/rinku/numempleados");
  }

  public consultarhorasjornada(idRoll) {
    return this.http.get(environment.apiRinku + "api/rinku/horas?"
      + "idRoll=" + idRoll);
  }

  public consultarverificarentregas(numEmpleado, fecha, numEmpleadoC) {
    return this.http.get(environment.apiRinku + "api/rinku/verificar/entregas?"
      + "numEmpleado=" + numEmpleado
      + "&fecha=" + fecha
      + "&numEmpleadoC=" + numEmpleadoC);
  }

  public guardarentregas($jsonEntregas) {
    return this.http.post(environment.apiRinku + "api/rinku/entregas",
      $jsonEntregas
    );
  }

  public consultarempleadosentregas(numEmpleado) {
    return this.http.get(environment.apiRinku + "api/rinku/empleados/entregas?"
      + "numEmpleado=" + numEmpleado);
  }

  public consultarregistrosentregas(numEmpleado, fecha) {
    return this.http.get(environment.apiRinku + "api/rinku/registros/entregas?"
      + "numEmpleado=" + numEmpleado
      + "&fecha=" + fecha);
  }

  public eliminarentregas(numEmpleado, fecha) {
    return this.http.delete(environment.apiRinku + "api/rinku/entregas?"
      + "numEmpleado=" + numEmpleado
      + "&fecha=" + fecha);
  }
}