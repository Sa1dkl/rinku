import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { EmpleadosService } from './../../services/empleados.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'consultaempleados.component.html',
  styleUrls: ['./consultaempleados.component.scss']
})
export class ConsultaempleadosComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: EmpleadosService) { }

  empleado: any;
  seleccionInsertar: any = [];

  public async ngOnInit() {
    document.oncontextmenu = function () { return false; }
    document.getElementById("tbEmpleados").style.display = "none";
    await this.consultarEmpleado();
  }

  seleccionarInsertar(e, tipo, rol) {
    this.seleccionInsertar = [];
    this.seleccionInsertar.push(e);
    this.seleccionInsertar.forEach(function (entry) {
      entry.tipo_empleado = tipo;
      entry.id_roll = rol;
    });
    if (this.seleccionInsertar.length > 1) {
      this.seleccionInsertar.shift();
    }
  }

  public async actualizarEmpleado() {
    var vEmpleado;
    var vNombre;
    var vApellidoP;
    var vApellidoM;
    var vCorreo;
    var vTipo;
    var vRol;
    var vFlag;

    this.seleccionInsertar.forEach(function (value) {
      vEmpleado = value.num_empleado;
      vNombre = value.nom_empleado;
      vApellidoP = value.ape_paterno;
      vApellidoM = value.ape_materno;
      vCorreo = value.nom_correo;
      vTipo = value.tipo_empleado;
      vRol = value.id_roll;
      vFlag = value.flag_activo;
    });

    let jsonEmpleados = JSON.stringify({
      'numEmpleado': vEmpleado,
      'nombre': vNombre,
      'apellidoP': vApellidoP,
      'apellidoM': vApellidoM,
      'correo': vCorreo,
      'tipo': vTipo,
      'roll': vRol,
      'flag': vFlag,
    });

    await swal({
      title: '¿Estás seguro de actualizar al empleado?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.service.actualizarempleados(jsonEmpleados).toPromise().then((Response: any) => {
          this.blockUI.stop();
          swal({
            type: 'success',
            title: 'Correcto',
            text: 'Empleado actualizado correctamente',
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        }, (error) => {
          this.blockUI.stop();
          swal({
            type: 'error',
            title: 'Error',
            text: "Error al actualizar intentelo de nuevo",
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        });
      }
    })
  }

  consultarEmpleado() {
    this.service.consultaempleados().subscribe((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.empleado = Response.data.response;
        this.empleado.forEach(function (entry) {
          entry.tipo_empleado = parseInt(entry.tipo_empleado);
          entry.id_roll = parseInt(entry.id_roll);
          entry.flag_activo = parseInt(entry.flag_activo);
          document.getElementById("tbEmpleados").style.display = "block";
        });
      } else {
        document.getElementById("tbEmpleados").style.display = "none";
        swal({
          type: 'info',
          title: 'Datos no encontrados',
          text: "Aun no cuenta con empleados registados, favor de registrar algun empleado.",
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      }
    });
  }
}
