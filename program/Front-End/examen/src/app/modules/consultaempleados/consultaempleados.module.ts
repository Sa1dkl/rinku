import {NgModule }from '@angular/core';
import {CommonModule }from '@angular/common';
import {ChartsModule }from 'ng2-charts/ng2-charts';
import {BsDropdownModule }from 'ngx-bootstrap/dropdown';
import {ConsultaempleadosComponent }from './consultaempleados.component';
import {ConsultaempleadosRoutingModule }from './consultaempleados-routing.module';
import {DataTableModule, GrowlModule, CodeHighlighterModule }from 'primeng/primeng';
import {MessageService }from 'primeng/components/common/messageservice';
import {MenubarModule }from 'primeng/menubar';
import {MenuItem }from 'primeng/api';
import {FieldsetModule}from 'primeng/fieldset';
import {FormsModule}from '@angular/forms';
import {RadioButtonModule}from 'primeng/radiobutton';
import {InputSwitchModule}from 'primeng/inputswitch';
import {TableModule}from 'primeng/table';
import {AutoCompleteModule}from 'primeng/autocomplete';
import {ButtonModule}from 'primeng/button';
import {DropdownModule}from 'primeng/dropdown';

@NgModule( {
  imports:[
    CommonModule,
    ConsultaempleadosRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    FieldsetModule,
    FormsModule,
    RadioButtonModule,
    InputSwitchModule,
    TableModule,
    AutoCompleteModule,
    ButtonModule,
    DropdownModule,
  ],
  declarations:[ConsultaempleadosComponent],
})
export class ConsultaempleadosModule {}
