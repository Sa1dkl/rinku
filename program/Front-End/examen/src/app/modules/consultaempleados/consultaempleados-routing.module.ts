import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsultaempleadosComponent } from './consultaempleados.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: ConsultaempleadosComponent,
    data: {
      title: 'consultaempleados',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class ConsultaempleadosRoutingModule { }
