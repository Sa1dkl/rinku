import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsultaentregasComponent } from './consultaentregas.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: ConsultaentregasComponent,
    data: {
      title: 'consultaentregas',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class ConsultaentregasRoutingModule { }
