import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { EntregasService } from './../../services/entregas.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'consultaentregas.component.html',
  styleUrls: ['./consultaentregas.component.scss']
})
export class ConsultaentregasComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: EntregasService) { }

  numEmpleado: any = '';
  filtro: any;
  filtered: any[];
  fechaCalendario: any = new Date();
  es: any;
  maxDateValue: Date = new Date();
  entregas: any;
  seleccionInsertar: any = [];
  btnDisaConsultar: any = true;

  ngOnInit() {
    document.oncontextmenu = function () { return false; }
    this.es = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
      dayNamesShort: ["Dom", "Lun", "Mar", "Mir", "Jue", "Vie", "Sab"],
      dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
      monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
      monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      today: 'Hoy',
      clear: 'Indefinida'
    };

    document.getElementById("entregasCubierto").style.display = "none";
    this.service.consultarempleadosfiltro().toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.filtro = Response.data.response;
      } else {
        swal({
          type: 'info',
          text: "Aun no cuenta empleados registrados,Favor de registrar antes de buscar.",
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      }
    });
  }

  filter(event) {
    this.filtered = [];
    if (this.filtro != undefined) {
      for (let i = 0; i < this.filtro.length; i++) {
        let numEmpleado = this.filtro[i].numEmpleado;
        if (numEmpleado.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
          this.filtered.push(numEmpleado);
        }
      }
    }
  }

  consultarEmpleado() {
    if (this.numEmpleado.length > 7) {
      this.btnDisaConsultar = false;
    } else {
      this.btnDisaConsultar = true;
    }
  }

  limpiar() {
    this.numEmpleado = '';
    this.fechaCalendario = new Date();
    this.entregas = [];
    this.btnDisaConsultar = true;
    document.getElementById("entregasCubierto").style.display = "none";
  }

  volverConsultar(vFecha) {
    document.getElementById("entregasCubierto").style.display = "none";
    vFecha = new Date(vFecha).toISOString().slice(0, 10);
    if (this.numEmpleado.length == 8) {
      this.service.consultarregistrosentregas(this.numEmpleado, vFecha).subscribe((Response: any) => {
        if (Response.data.response[0] != undefined) {
          this.entregas = Response.data.response;
          document.getElementById("entregasCubierto").style.display = "block";
        } else {
          document.getElementById("entregasCubierto").style.display = "none";
        }
      }, (error) => {
        this.blockUI.stop();
        swal({
          type: 'error',
          title: 'Error',
          text: 'Error al guardar intentelo de nuevo',
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      });
    }
  }

  consultarRegistros(vFecha) {
    document.getElementById("entregasCubierto").style.display = "none";
    vFecha = new Date(vFecha).toISOString().slice(0, 10);
    if (this.numEmpleado.length == 8) {
      this.service.consultarregistrosentregas(this.numEmpleado, vFecha).subscribe((Response: any) => {
        if (Response.data.response[0] != undefined) {
          this.entregas = Response.data.response;
          document.getElementById("entregasCubierto").style.display = "block";
        } else {
          document.getElementById("entregasCubierto").style.display = "none";
          swal({
            type: 'info',
            text: "No se encontró información, favor de consultar con otros datos",
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        }
      }, (error) => {
        this.blockUI.stop();
        swal({
          type: 'error',
          title: 'Error',
          text: 'Error al guardar intentelo de nuevo',
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      });
    }
  }

  seleccionarInsertar(e) {
    this.seleccionInsertar = [];
    this.seleccionInsertar.push(e);
    if (this.seleccionInsertar.length > 1) {
      this.seleccionInsertar.shift();
    }
  }

  public async eliminarEntrega() {
    var vFecha;
    this.seleccionInsertar.forEach(function (entry) {
      vFecha = entry.fec_movimiento;
    });
    await swal({
      title: '¿Está seguro de eliminar la entrega?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.service.eliminarentregas(this.numEmpleado, vFecha).toPromise().then((Response: any) => {
          this.volverConsultar(vFecha);
          this.blockUI.stop();
          swal({
            type: 'success',
            title: 'Correcto',
            text: 'Entrega eliminada correctamente',
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        }, (error) => {
          this.blockUI.stop();
          swal({
            type: 'error',
            title: 'Error',
            text: "Error al eliminar la entrega intentelo de nuevo",
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        });
      }
    })
    await this.volverConsultar(vFecha);
  }
}
