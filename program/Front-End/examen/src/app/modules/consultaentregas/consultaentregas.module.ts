import {NgModule }from '@angular/core';
import {CommonModule }from '@angular/common';
import {ChartsModule }from 'ng2-charts/ng2-charts';
import {BsDropdownModule }from 'ngx-bootstrap/dropdown';
import {ConsultaentregasComponent }from './consultaentregas.component';
import {ConsultaentregasRoutingModule }from './consultaentregas-routing.module';
import {DataTableModule, GrowlModule, CodeHighlighterModule }from 'primeng/primeng';
import {MessageService }from 'primeng/components/common/messageservice';
import {MenubarModule }from 'primeng/menubar';
import {MenuItem }from 'primeng/api';
import {FieldsetModule}from 'primeng/fieldset';
import {FormsModule}from '@angular/forms';
import {RadioButtonModule}from 'primeng/radiobutton';
import {InputSwitchModule}from 'primeng/inputswitch';
import {TableModule}from 'primeng/table';
import {AutoCompleteModule}from 'primeng/autocomplete';
import {CalendarModule}from 'primeng/calendar';
import {SliderModule}from 'primeng/slider';
import {ButtonModule}from 'primeng/button';

@NgModule( {
  imports:[
    CommonModule,
    ConsultaentregasRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    FieldsetModule,
    FormsModule,
    RadioButtonModule,
    InputSwitchModule,
    TableModule,
    AutoCompleteModule,
    CalendarModule,
    SliderModule,
    ButtonModule,
  ],
  declarations:[ConsultaentregasComponent],
})
export class ConsultaentregasModule {}
