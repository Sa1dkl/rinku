import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { GeneranominaService } from './../../services/generanomina.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'generanomina.component.html',
  styleUrls: ['./generanomina.component.scss']
})
export class GeneranominaComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: GeneranominaService) { }

  es: any;
  fechaCalendario: any = new Date();
  maxDateValue: any = new Date();
  filtro: any;
  flag: any;

  public async ngOnInit() {
    document.oncontextmenu = function () { return false; }
    this.es = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
      dayNamesShort: ["Dom", "Lun", "Mar", "Mir", "Jue", "Vie", "Sab"],
      dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
      monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
      monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      today: 'Hoy',
      clear: 'Indefinida'
    };

    await this.service.consultarempleadosfiltro().toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.filtro = Response.data.response;
      } else {
        swal({
          type: 'info',
          text: "Aun no cuenta con empleados registados, favor de registrar alguno",
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      }
    });
  }

  public async Generarnomina(vFecha) {
    await swal({
      title: '¿Está seguro de generar nómina?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        vFecha = new Date(vFecha).toISOString().slice(0, 10);
        this.blockUI.start('Generando...');
        this.service.consultarflagentregas(vFecha).toPromise().then((Response: any) => {
          this.flag = Response.data.response[0].flag;
          if (this.flag == 1) {
            this.service.generarnominas(vFecha).toPromise().then((Response: any) => {
              this.blockUI.stop();
              swal({
                type: 'success',
                title: 'Se generó correctamente',
                showConfirmButton: true,
                allowOutsideClick: false,
                allowEscapeKey: false
              })
            }, (error) => {
              this.blockUI.stop();
              swal({
                type: 'error',
                title: 'Error',
                text: 'Error al generar intentelo de nuevo',
                showConfirmButton: true,
                allowOutsideClick: false,
                allowEscapeKey: false
              })
            });
          } else {
            this.blockUI.stop();
            swal({
              type: 'info',
              text: "Aun no cuenta con entregas de empleados en la fecha seleccionada",
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          }
          this.blockUI.stop();
        });
      }
    })
  }
}
