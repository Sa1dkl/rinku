import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GeneranominaComponent } from './generanomina.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: GeneranominaComponent,
    data: {
      title: 'generanomina',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class GeneranominaRoutingModule { }
