import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { GeneranominaComponent } from './generanomina.component';
import { GeneranominaRoutingModule } from './generanomina-routing.module';
import { DataTableModule, GrowlModule, CodeHighlighterModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import { FieldsetModule } from 'primeng/fieldset';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  imports: [
    CommonModule,
    GeneranominaRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    FieldsetModule,
    FormsModule,
    CalendarModule,
  ],
  declarations: [GeneranominaComponent],
})
export class GeneranominaModule { }
