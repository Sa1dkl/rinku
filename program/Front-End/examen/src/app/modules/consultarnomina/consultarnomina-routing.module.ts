import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsultarnominaComponent } from './consultarnomina.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: ConsultarnominaComponent,
    data: {
      title: 'consultarnomina',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class ConsultarnominaRoutingModule {}
