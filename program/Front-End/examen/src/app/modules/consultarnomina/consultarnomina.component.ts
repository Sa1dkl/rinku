import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { GeneranominaService } from './../../services/generanomina.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'consultarnomina.component.html',
  styleUrls: ['./consultarnomina.component.scss']
})
export class ConsultarnominaComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: GeneranominaService) { }

  fechaCalendario:any = new Date();
  maxDateValue:any = new Date();
  nominas:any;

  public async ngOnInit() {
    document.oncontextmenu = function () { return false; }

    document.getElementById("nominas").style.display = "none";

    this.service.consultarnominas().toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
      this.nominas = Response.data.response;
      document.getElementById("nominas").style.display = "block";
    } else {
      document.getElementById("nominas").style.display = "none";
      swal({
        type: 'info',
        text: "Aun no cuenta con informacion de nominas",
        showConfirmButton: true,
        allowOutsideClick: false,
        allowEscapeKey: false
      })
    }
  });
  }

  private descargar(base64: any, nombre: string) {
    const link = 'data:application/pdf;base64,' + base64;
    const descargarLink = document.createElement("a");
    const fileName = nombre;
    descargarLink.href = link;
    descargarLink.download = fileName;
    descargarLink.click();
  }

  public async generarpdf() {
    this.blockUI.start('Generando...');
    await this.service.generarpdf().toPromise().then((Response: any) => {
      this.descargar(Response.data.response.base64, Response.data.response.nombre);
      this.blockUI.stop();
    }, (error) => {
      this.blockUI.stop();
      swal({
        type: 'error',
        title: 'Error',
        text: "Error al generar el pdf intentelo de nuevo mas tarde",
        showConfirmButton: true,
        allowOutsideClick: false,
        allowEscapeKey: false
      })
    });
  }
}
