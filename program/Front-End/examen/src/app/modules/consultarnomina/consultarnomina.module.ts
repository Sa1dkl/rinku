import {NgModule }from '@angular/core';
import {CommonModule }from '@angular/common';
import {ChartsModule }from 'ng2-charts/ng2-charts';
import {BsDropdownModule }from 'ngx-bootstrap/dropdown';
import {ConsultarnominaComponent }from './consultarnomina.component';
import {ConsultarnominaRoutingModule }from './consultarnomina-routing.module';
import {DataTableModule, GrowlModule, CodeHighlighterModule }from 'primeng/primeng';
import {TableModule}from 'primeng/table';
import {AutoCompleteModule}from 'primeng/autocomplete';
import {FormsModule}from '@angular/forms';
import {CalendarModule}from 'primeng/calendar';
import {DropdownModule}from 'primeng/dropdown';
import {MultiSelectModule}from 'primeng/multiselect';
import {MenubarModule }from 'primeng/menubar';
import {MenuItem }from 'primeng/api';

@NgModule( {
  imports:[
    CommonModule,
    ConsultarnominaRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    TableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    AutoCompleteModule,
    FormsModule,
    CalendarModule,
    DropdownModule,
    MultiSelectModule,
  ],
  declarations:[ConsultarnominaComponent],
})
export class ConsultarnominaModule {}
