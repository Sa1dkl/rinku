import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmpeladosComponent } from './empelados.component';

const APP_ROUTES: Routes = [{
  path: '',
  component: EmpeladosComponent,
  data: {
    title: 'empelados',
  },
},
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class EmpeladosRoutingModule { }
