import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { EmpleadosService } from './../../services/empleados.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'empelados.component.html',
  styleUrls: ['./empleados.component.scss']
})
export class EmpeladosComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: EmpleadosService) { }
  roll: any = 1;
  tipo: any = 1;
  nombre: any;
  apellidoP: any;
  apellidoM: any;
  correo: any;
  flagActivo: any;
  numEmpleado: any;
  flagCorreo: any;

  ngOnInit() {
    this.consultaNumeroEmpleado();
    document.oncontextmenu = function () { return false; }
    var vNombre = document.getElementById('vNombre');
    vNombre.oncopy = function (e) {
      e.preventDefault();
    }
    vNombre.oncut = function (e) {
      e.preventDefault();
    }
    vNombre.onpaste = function (e) {
      e.preventDefault();
    }
    var vApellidoP = document.getElementById('vApellidoP');
    vApellidoP.oncopy = function (e) {
      e.preventDefault();
    }
    vApellidoP.oncut = function (e) {
      e.preventDefault();
    }
    vApellidoP.onpaste = function (e) {
      e.preventDefault();
    }
    var vApellidoM = document.getElementById('vApellidoM');
    vApellidoM.oncopy = function (e) {
      e.preventDefault();
    }
    vApellidoM.oncut = function (e) {
      e.preventDefault();
    }
    vApellidoM.onpaste = function (e) {
      e.preventDefault();
    }
    var vCorreo = document.getElementById('vCorreo');
    vCorreo.oncopy = function (e) {
      e.preventDefault();
    }
    vCorreo.oncut = function (e) {
      e.preventDefault();
    }
    vCorreo.onpaste = function (e) {
      e.preventDefault();
    }

  }

  public async agregarEmpleado(vEmpleado, vNombre, vApellidoP, vApellidoM, vCorreo) {
    await swal({
      title: '¿Está seguro de agregar al empleado?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.service.consultarcorreos(vCorreo).toPromise().then((Response: any) => {
          if (Response.data.response[0] != undefined) {
            this.flagCorreo = Response.data.response[0].flagCorreo;
          }
        });
        let jsonEmpleados = JSON.stringify({
          'numEmpleado': vEmpleado,
          'nombre': vNombre,
          'apellidoP': vApellidoP,
          'apellidoM': vApellidoM,
          'correo': vCorreo,
          'tipo': this.tipo,
          'roll': this.roll,
        });
        if (this.flagCorreo == 0) {
           this.service.altaempleados(jsonEmpleados).toPromise().then((Response: any) => {
            this.blockUI.stop();
            swal({
              type: 'success',
              title: 'Empleado registrado correctamente',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          }, (error) => {
            this.blockUI.stop();
            swal({
              type: 'error',
              title: 'Error',
              text: 'Error al insertar intentelo de nuevo',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          });
          this.consultaNumeroEmpleado();
          this.nombre = '';
          this.apellidoP = '';
          this.apellidoM = '';
          this.correo = '';
          this.tipo = 1;
          this.roll = 1;
        } else {
          swal({
            type: 'info',
            text: "El correo ya existe, favor de elegir otro",
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        }
      }
    })
  }

  public async consultaNumeroEmpleado() {
    await this.service.consultanumEmpelado().toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.numEmpleado = Response.data.response[0].numEmpleado;
      }
    });
  }

  limpiar() {
    this.nombre = '';
    this.apellidoP = '';
    this.apellidoM = '';
    this.correo = '';
    this.tipo = 1;
    this.roll = 1;
    this.consultaNumeroEmpleado();
  }
}
