import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { EmpeladosComponent } from './empelados.component';
import { EmpeladosRoutingModule } from './empelados-routing.module';
import { DataTableModule, GrowlModule, CodeHighlighterModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import { FieldsetModule } from 'primeng/fieldset';
import { FormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';

@NgModule({
  imports: [
    CommonModule,
    EmpeladosRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    FieldsetModule,
    FormsModule,
    RadioButtonModule,
  ],
  declarations: [EmpeladosComponent],
})
export class EmpeladosModule { }
