import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ExamenComponent } from './examen.component';
import { ExamenRoutingModule } from './examen-routing.module';
import { DataTableModule, GrowlModule, CodeHighlighterModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';

@NgModule({
  imports: [
    CommonModule,
    ExamenRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
  ],
  declarations: [ExamenComponent],
})
export class ExamenModule {}
