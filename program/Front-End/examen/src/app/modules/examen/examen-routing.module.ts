import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ExamenComponent } from './examen.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: ExamenComponent,
    data: {
      title: 'examen',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class ExamenRoutingModule {}
