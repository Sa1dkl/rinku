import { ICiudad } from './../models/ciudad';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../../services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CiudadService {
  public apiRoot: string;

  constructor(public http: HttpClient, public configService: ConfigService) {
    const config = configService.getConfig();
    this.apiRoot = config.webApiBaseUrl + '/ciudads';
  }

  obtener(parametros?: any): Observable<any> {
    return this.http.get(this.apiRoot, {params: parametros});
  }

  obtenerPorId(id): Observable<any> {
    return this.http.get(`${this.apiRoot}/${id}`);
  }

  guardar(ciudad: ICiudad): Observable<any> {
    return this.http.post(this.apiRoot, ciudad);
  }

  actualizar(ciudad: ICiudad): Observable<any> {
    return this.http.put(`${this.apiRoot}/${ciudad.id}`, ciudad);
  }

  eliminar(id): Observable<any> {
    return this.http.delete(`${this.apiRoot}/${id}`);
  }
}
