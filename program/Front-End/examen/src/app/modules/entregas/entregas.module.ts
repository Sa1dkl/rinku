import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { EntregasComponent } from './entregas.component';
import { EntregasRoutingModule } from './entregas-routing.module';
import { DataTableModule, GrowlModule, CodeHighlighterModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import {FieldsetModule} from 'primeng/fieldset';
import {FormsModule} from '@angular/forms';
import {RadioButtonModule} from 'primeng/radiobutton';
import {InputSwitchModule} from 'primeng/inputswitch';
import {TableModule} from 'primeng/table';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {CalendarModule} from 'primeng/calendar';
import {SliderModule} from 'primeng/slider';

@NgModule({
  imports: [
    CommonModule,
    EntregasRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    FieldsetModule,
    FormsModule,
    RadioButtonModule,
    InputSwitchModule,
    TableModule,
    AutoCompleteModule,
    CalendarModule,
    SliderModule,
  ],
  declarations: [EntregasComponent],
})
export class EntregasModule {}
