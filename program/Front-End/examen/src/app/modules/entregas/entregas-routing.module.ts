import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EntregasComponent } from './entregas.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: EntregasComponent,
    data: {
      title: 'entregas',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class EntregasRoutingModule { }
