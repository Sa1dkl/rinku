import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { EntregasService } from './../../services/entregas.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'entregas.component.html',
  styleUrls: ['./entregas.component.scss']
})
export class EntregasComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: EntregasService) { }
  rol: any;
  tipo: any;
  nombre: any;
  rolCubrio: any;
  tipoCubrio: any;
  nombreCubrio: any;
  flagCubrio: any = false;
  validarFlagCubrio: any;
  numEmpleado: any = '';
  filtro: any;
  filtroCubrePuesto: any;
  filtered: any[];
  filteredCubrePuesto: any[];
  fechaCalendario: any = new Date();
  es: any;
  entregas: any = 10;
  contraerTurno: any = true;
  numEmpleadoCubrio: any = '';
  cubrepuestos: any;
  iSwitch: any = true;
  maxDateValue:any;
  flagVerificarEntradas:any;
  horas:any;
  btnDisaGuardar:any = true;

  public async ngOnInit() {
    document.oncontextmenu = function () { return false; }
    this.maxDateValue = new Date();
    this.es = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
      dayNamesShort: ["Dom", "Lun", "Mar", "Mir", "Jue", "Vie", "Sab"],
      dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
      monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
      monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      today: 'Hoy',
      clear: 'Indefinida'
    };

    await this.service.consultarempleadosfiltro().toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.filtro = Response.data.response;
      } else {
        swal({
          type: 'info',
          title: 'Datos no encontrados',
          text: "Aun no cuenta con empleados registados, favor de registrar alguno",
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      }
    });
    await this.service.consultarempleadosfiltrocubrepuestos().toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.filtroCubrePuesto = Response.data.response;
      }
    });
  }

  validarCubrio() {
    if (this.flagCubrio == true) {
      this.btnDisaGuardar = true;
      this.contraerTurno = false;
    } else {
      this.contraerTurno = true;
      this.numEmpleadoCubrio = '';
      this.nombreCubrio = '';
      this.tipoCubrio = '';
      this.rolCubrio = '';
      this.btnDisaGuardar = false;
    }
  }

  filter(event) {
    this.filtered = [];
    if(this.filtro != undefined)
    {
    for (let i = 0; i < this.filtro.length; i++) {
      let numEmpleado = this.filtro[i].numEmpleado;
      if (numEmpleado.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filtered.push(numEmpleado);
      }
    }
  }
  }

  filterCubrioPuesto(event) {
    this.filteredCubrePuesto = [];
    for (let i = 0; i < this.filtroCubrePuesto.length; i++) {
      let numEmpleado = this.filtroCubrePuesto[i].numEmpleado;
      if (this.numEmpleadoCubrio.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredCubrePuesto.push(numEmpleado);
      }
    }
  }
  public async guardarEntregas(vFecha) {
      await this.service.consultarhorasjornada(this.numEmpleado).toPromise().then((Response: any) => {
        if (Response.data.response[0] != undefined) {
          this.horas = Response.data.response[0].num_horas;
        }
      });
    vFecha = new Date(vFecha).toISOString().slice(0, 10);
    let jsonEntregas = JSON.stringify({
      'numEmpleado': this.numEmpleado,
      'nomEmpleado': this.nombre,
      'fecha': vFecha,
      'numEntregas': this.entregas,
      'horas':  this.horas ,
      'numEmpleadoCubrio': this.numEmpleadoCubrio,
      'nomEmpleadoCubrio': this.nombreCubrio,
      'flag': this.flagCubrio,
    });
    await this.service.consultarverificarentregas(this.numEmpleado,vFecha,this.numEmpleadoCubrio).toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
      this.flagVerificarEntradas = Response.data.response[0].flagExiste;
      }
    });
    if(this.flagVerificarEntradas == 0){
      await swal({
        title: '¿Está seguro de guardar las entregas?',
        text: "",
        type: 'question',
        showCancelButton: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'No',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.value) {
       this.service.guardarentregas(jsonEntregas).toPromise().then((Response: any) => {
        this.blockUI.stop();
        this.nombre = '';
        this.tipo = '';
        this.rol = '';
        this.nombreCubrio = '';
        this.tipoCubrio = '';
        this.rolCubrio = '';
        this.entregas = 10;
        this.iSwitch = true;
        this.contraerTurno = true;
        this.numEmpleado = '';
        this.numEmpleadoCubrio = '';
        this.flagCubrio = false;
        this.fechaCalendario = new Date();
        swal({
          type: 'success',
          title: 'Entregas guardadas correctamente',
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      }, (error) => {
        this.blockUI.stop();
        swal({
          type: 'error',
          title: 'Error',
          text: 'Error al guardar intentelo de nuevo',
          showConfirmButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false
        })
      });
    }
  })
  }
  if(this.flagVerificarEntradas == 1){
    swal({
      type: 'info',
      text: "El empleado que intenta guardar, ya realizo entregas el dia seleccionado",
      showConfirmButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false
    })
  }
  if(this.flagVerificarEntradas == 2){
    swal({
      type: 'info',
      text: "El empleado que intenta cubrir ya registro entregas",
      showConfirmButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false
    })
  }
  }

  limpiar() {
    this.nombre = '';
    this.tipo = '';
    this.rol = '';
    this.nombreCubrio = '';
    this.tipoCubrio = '';
    this.rolCubrio = '';
    this.entregas = 10;
    this.iSwitch = true;
    this.contraerTurno = true;
    this.numEmpleado = '';
    this.numEmpleadoCubrio = '';
    this.flagCubrio = false;
    this.fechaCalendario = new Date();
  }

  consultarEmpleado() {
    if (this.numEmpleado.length == 8) {
      this.service.consultarempleadosentregas(this.numEmpleado).subscribe((Response: any) => {
        if (Response.data.response[0] != undefined) {
          this.btnDisaGuardar = false;
          this.nombre = Response.data.response[0].nombre;
          this.rol = Response.data.response[0].des_roll;
          this.tipo = Response.data.response[0].des_tipoempleado;
          this.validarFlagCubrio = parseInt(Response.data.response[0].flag_cubrepuesto);
          if (this.validarFlagCubrio == 1) {
            this.iSwitch = false;
          }
        } else {
          swal({
            type: 'info',
            text: "No se encontró información del empleado, favor de verificar si existe el empleado.",
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        }
      });
    } else {
      this.btnDisaGuardar = true;
      this.nombre = '';
      this.tipo = '';
      this.rol = '';
      this.nombreCubrio = '';
      this.tipoCubrio = '';
      this.rolCubrio = '';
      this.entregas = 10;
      this.iSwitch = true;
      this.contraerTurno = true;
      this.flagCubrio = false;
      this.fechaCalendario = new Date();
    }
  }

  consultarEmpleadoCubrio() {
    if (this.numEmpleadoCubrio.length == 8) {
      this.service.consultarempleadosentregas(this.numEmpleadoCubrio).subscribe((Response: any) => {
        if (Response.data.response[0] != undefined) {
          this.validarFlagCubrio = parseInt(Response.data.response[0].flag_cubrepuesto);
          if (this.validarFlagCubrio == 1) {
            swal({
              type: 'info',
              text: "El empleado que busco no se puede suplir.",
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          } else {
            this.nombreCubrio = Response.data.response[0].nombre;
            this.rolCubrio = Response.data.response[0].des_roll;
            this.tipoCubrio = Response.data.response[0].des_tipoempleado;
            this.btnDisaGuardar = false;
          }
        } else {
          swal({
            type: 'info',
            text: "No se encontró información del empleado, favor de intentar nuevamente o verificar el numero de empleado.",
            showConfirmButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
          })
        }
      });
    } else {
      this.btnDisaGuardar = true;
      this.nombreCubrio = '';
      this.tipoCubrio = '';
      this.rolCubrio = '';
    }
  }
}
