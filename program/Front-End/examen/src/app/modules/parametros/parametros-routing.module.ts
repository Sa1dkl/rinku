import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ParametrosComponent } from './parametros.component';

const APP_ROUTES: Routes = [{
  path: '',
  component: ParametrosComponent,
  data: {
    title: 'parametros',
  },
},
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule],
})
export class ParametrosRoutingModule { }
