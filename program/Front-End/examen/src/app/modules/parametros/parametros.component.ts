import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { esLocale } from 'ngx-bootstrap/locale';
import { ParametrosService } from './../../services/parametros.service';
import swal from 'sweetalert2';
import { MenuService, AuthenticationService } from '../../services';

@Component({
  templateUrl: 'parametros.component.html',
  styleUrls: ['./parametros.component.scss']
})
export class ParametrosComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private router: Router, private service: ParametrosService) { }

  roles: any;
  bonos: any;
  sueldo: any;
  horas: any;
  porcentajeDespenza: any;
  roll: any;
  bono: any;
  valor: any;
  isr: any;

  public async ngOnInit() {
    document.oncontextmenu = function () { return false; }
    await this.consultarolesCombo();
    this.roles = this.consulta();
    await this.consultarolesComboBono();
    this.bonos = this.consultaBonos();

    this.consutarRoles(1);
    this.consutarBonos(1);
    this.consutarIsr(1);

  }

  public async consultarolesCombo() {
    await this.service.consultaroles().toPromise().then((Response: any) => {
      this.roles = Response.data.response;
    });
  }

  public async consultarolesComboBono() {
    await this.service.consultabonos().toPromise().then((Response: any) => {
      this.bonos = Response.data.response;
    });
  }

  public async consutarRoles(roles) {
    await this.service.consultasueldos(roles).toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.sueldo = Response.data.response[0].num_sueldo_hora;
        this.horas = Response.data.response[0].num_horas;
        this.porcentajeDespenza = Response.data.response[0].prc_despensa;
      }
    });
  }

  public async consutarBonos(bonos) {
    await this.service.consultasparametrosbonos(bonos).toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.roll = Response.data.response[0].des_roll;
        this.bono = Response.data.response[0].num_bono;
      }
    });
  }

  validarFormulario(vRoles) {
    this.consutarBonos(vRoles);
    (<HTMLInputElement>document.getElementById('vBonos')).value = vRoles;
  }

  consulta() {
    return this.roles;
  }

  consultaBonos() {
    return this.bonos;
  }

  public async consutarIsr(vValidar) {
    await this.service.consultarisr(vValidar).toPromise().then((Response: any) => {
      if (Response.data.response[0] != undefined) {
        this.isr = Response.data.response[0].prc_isr;
        this.valor = Response.data.response[0].num_isr;
      }
    });
  }

  public async  ActualizarBonos(vBonos) {
    await swal({
      title: '¿Está seguro de actualizar los bonos?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        let jsonBonos = JSON.stringify({
          'idBono': vBonos,
          'bono': this.bono,
        });
        if (this.bono.length >= 1) {
          this.blockUI.start('Actualizando...');
          this.service.actualizarbonos(jsonBonos).toPromise().then((Response: any) => {
            this.blockUI.stop();
            swal({
              type: 'success',
              title: 'Se actualizó correctamente',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          }, (error) => {
            this.blockUI.stop();
            swal({
              type: 'error',
              title: 'Error',
              text: 'Error al actualizar intentelo de nuevo',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          });
        } else {
          swal({
            type: 'info',
            text: 'Los campos deben de contener algun valor',
            allowOutsideClick: false,
            allowEscapeKey: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
          })
        }
      }
    })
  }

  public async ActualizarSueldos(vRoles) {
    await swal({
      title: '¿Está seguro de actualizar el sueldo?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        let jsonSuedos = JSON.stringify({
          'idRoll': vRoles,
          'sueldo': this.sueldo,
          'horas': this.horas,
          'porcentaje': this.porcentajeDespenza,
        });
        if (this.sueldo.length >= 1 && this.horas.length >= 1 && this.porcentajeDespenza.length >= 1) {
          this.blockUI.start('Actualizando...');
          this.service.actualizarsueldos(jsonSuedos).toPromise().then((Response: any) => {
            this.blockUI.stop();
            swal({
              type: 'success',
              title: 'Se actualizó correctamente',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          }, (error) => {
            this.blockUI.stop();
            swal({
              type: 'error',
              title: 'Error',
              text: 'Error al actualizar intentelo de nuevo',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          });
        } else {
          swal({
            type: 'info',
            text: 'Los campos deben de contener algun valor',
            allowOutsideClick: false,
            allowEscapeKey: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
          })
        }
      }
    })
  }

  public async ActualizarIsr(vValidar) {
    await swal({
      title: '¿Está seguro de actualizar el ISR?',
      text: "",
      type: 'question',
      showCancelButton: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        let jsonIsr = JSON.stringify({
          'validar': vValidar,
          'valor': this.valor,
          'isr': this.isr,
        });

        if (this.valor.length >= 1 && this.isr.length >= 1) {
          this.blockUI.start('Actualizando...');
          this.service.actualizarisr(jsonIsr).toPromise().then((Response: any) => {
            this.blockUI.stop();
            swal({
              type: 'success',
              title: 'Se actualizó correctamente',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          }, (error) => {
            this.blockUI.stop();
            swal({
              type: 'error',
              title: 'Error',
              text: 'Error al actualizar intentelo de nuevo',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false
            })
          });
        } else {
          swal({
            type: 'info',
            text: 'Los campos deben de contener algun valor',
            allowOutsideClick: false,
            allowEscapeKey: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
          })
        }
      }
    })
  }

}
