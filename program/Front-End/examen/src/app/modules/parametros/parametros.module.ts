import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ParametrosComponent } from './parametros.component';
import { ParametrosRoutingModule } from './parametros-routing.module';
import { DataTableModule, GrowlModule, CodeHighlighterModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import {FieldsetModule} from 'primeng/fieldset';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ParametrosRoutingModule,
    ChartsModule,
    BsDropdownModule,
    DataTableModule,
    GrowlModule,
    CodeHighlighterModule,
    MenubarModule,
    FieldsetModule,
    FormsModule,
  ],
  declarations: [ParametrosComponent],
})
export class ParametrosModule {}
