import { tap } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(public auth: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    const token = sessionStorage.getItem('token');
    if (request.url !== environment.STATIC.webbridge && (currentUser && token)) {
      request = request.clone({
        setHeaders: {
          Authorization: `${token}`,
        },
      });
    }
    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            return true;
          }
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse && (err.status === 403 || err.status === 401)) {
            this.auth.logout();
          }
        }
      )
    );
  }
}
