import { environment } from './../environments/environment';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

// Import Containers
import { FullLayoutComponent, SimpleLayoutComponent } from './containers';
import { AuthGuard } from './guards/Auth.guard';

export const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'examen',
    pathMatch: 'full',
  },
  {
    canActivate: [AuthGuard],
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home',
    },
    children: [
      {
        path: 'examen',
        loadChildren: './modules/examen/examen.module#ExamenModule',
      },
      {
        path: 'parametros',
        loadChildren: './modules/parametros/parametros.module#ParametrosModule',
      },
      {
        path: 'empelados',
        loadChildren: './modules/empelados/empelados.module#EmpeladosModule',
      },
      {
        path: 'consultaempleados',
        loadChildren: './modules/consultaempleados/consultaempleados.module#ConsultaempleadosModule',
      },
      {
        path: 'entregas',
        loadChildren: './modules/entregas/entregas.module#EntregasModule',
      },
      {
        path: 'consultaentregas',
        loadChildren: './modules/consultaentregas/consultaentregas.module#ConsultaentregasModule',
      },
      {
        path: 'generanomina',
        loadChildren: './modules/generanomina/generanomina.module#GeneranominaModule',
      },
      {
        path: 'consultarnomina',
        loadChildren: './modules/consultarnomina/consultarnomina.module#ConsultarnominaModule',
      },
    ],
  },
  {
    path: 'login',
    component: SimpleLayoutComponent,
    data: {
      title: 'Login',
    },
    children: [
      {
        path: '',
        loadChildren: './modules/login/login.module#LoginModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {
  constructor(private router: Router) {
    if (environment.production) {
      this.router.errorHandler = (error: any) => {
        this.router.navigate(['/examen']); // or redirect to default route
      };
    }
  }
}
