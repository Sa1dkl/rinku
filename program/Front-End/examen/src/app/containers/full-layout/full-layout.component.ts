import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import {ToasterConfig} from 'angular2-toaster';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
})
export class FullLayoutComponent implements OnInit {
  public imgUrl: string;
  public appLogo: string;
  public user: any;
  public notificaciones: any [];
  public mensajes: any [];
  public haveMenu: boolean;

  public toasterconfig: ToasterConfig = new ToasterConfig({animation: 'slideDown'});

  constructor(private auth: AuthenticationService) {
    this.notificaciones = [];
    this.mensajes = [];
  }

  ngOnInit() {

    this.haveMenu = environment.haveMenu;
    this.appLogo = environment.appLogo;
  }
}
