import { TallerPage } from './taller.po';
import { browser, element } from 'protractor';

describe('core-ui App', function() {
  let page: TallerPage;

  beforeEach(() => {
    page = new TallerPage();
  });

  it('should exist component Taller', async() => {
    page.navigateTo();
    expect(element('app-taller').isPresent()).toBeTruthy();
  });
});
