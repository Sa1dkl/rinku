<?php

namespace Coppel\Rinku\Controllers;

use Coppel\RAC\Controllers\RESTController;
use Coppel\RAC\Exceptions\HTTPException;
use Coppel\Rinku\Models as Modelos;

class ApiController extends RESTController
{
    private $logger;
    private $modelo;

    public function onConstruct()
    {
        $this->logger = \Phalcon\DI::getDefault()->get('logger');
        $this->modelo = new Modelos\ApiModel();
    }


    //** funcion para generar nominas */
    public function generarNominas()
    {
        $response = null;
        $fecha = $_GET['fecha'];

        try {
           $response = $this->modelo->generarNominas($fecha);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar nominas */
    public function consultarNominas()
    {
        $response = null;
        try {
           $response = $this->modelo->consultarNominas();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar si existe el correo */
    public function consultarFlagCorreos()
    {
        $response = null;
        $correo = $_GET['correo'];
        try {
           $response = $this->modelo->consultarFlagCorreos($correo);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar los roles */
    public function consultarRoles()
    {
        $response = null;
        try {
           $response = $this->modelo->consultarRoles();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para consultar los bonos */
    public function consultarBonos()
    {
        $response = null;
        try {
           $response = $this->modelo->consultarBonos();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para consultar los sueldos */
    public function consultarSueldos()
    {
        $response = null;
        $idRoll = $_GET['idRoll'];
        try {
           $response = $this->modelo->consultarSueldos($idRoll);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar los bonos para el combo*/
    public function consultarParametrosBonos()
    {
        $response = null;
        $idBono = $_GET['idBono'];
        try {
           $response = $this->modelo->consultarParametrosBonos($idBono);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para verificar si el empleado ya realizo entregas*/
    public function consultarVerificarEntregas()
    {
        $response = null;
        $numEmpleado = $_GET['numEmpleado'];
        $fecha = $_GET['fecha'];
        $numEmpleadoC = $_GET['numEmpleadoC'];
        try {
           $response = $this->modelo->consultarVerificarEntregas($numEmpleado,$fecha,$numEmpleadoC);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar las horas de la jornada de trabajo parametrizadas*/
    public function consultarHoras()
    {
        $response = null;
        $idRoll = $_GET['idRoll'];
        try {
           $response = $this->modelo->consultarHoras($idRoll);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar las entregas realizadas por empelado*/
    public function consultarRegistrosEntregas()
    {
        $response = null;
        $numEmpleado = $_GET['numEmpleado'];
        $fecha = $_GET['fecha'];
        try {
           $response = $this->modelo->consultarRegistrosEntregas($numEmpleado,$fecha);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para consultar los empleados registrados*/
    public function consultarEmpledos()
    {
        $response = null;
        try {
           $response = $this->modelo->consultarEmpledos();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para consultar el isr parametrizado*/
    public function consultarIsr()
    {
        $response = null;
        $validar = $_GET['validar'];
        try {
           $response = $this->modelo->consultarIsr($validar);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para consultar el proximo numero de empleado disponible a insertar*/
    public function consultarNumEmpledos()
    {
        $response = null;
        try {
           $response = $this->modelo->consultarNumEmpledos();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para ver el filtro de empleados ya agregados*/
    public function consultarEmpledosFiltros()
    {
        $response = null;

        try {
            $response = $this->modelo->consultarEmpledosFiltros();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para ver el filtro de empleados ya agregados si auxiliares*/
    public function consultarEmpledosFiltrosPuestos()
    {
        $response = null;

        try {
            $response = $this->modelo->consultarEmpledosFiltrosPuestos();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

   //** funcion para optener la informacion del empleado*/
    public function consultarEmpleadoEntregas()
    {
        $response = null;
        $numEmpleado = $_GET['numEmpleado'];

        try {
            $response = $this->modelo->consultarEmpleadoEntregas($numEmpleado);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para ver si existen entregas*/
     public function consultarFlagEntregas()
     {
         $response = null;
         $fecha = $_GET['fecha'];
 
         try {
             $response = $this->modelo->consultarFlagEntregas($fecha);
         } catch (\Exception $ex) {
             $mensaje = $ex->getMessage();
             $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
             throw new HTTPException(
                 'No fue posible completar su solicitud, intente de nuevo por favor.',
                 500, [
                     'dev' => $mensaje,
                     'internalCode' => 'SIE1000',
                     'more' => 'Verificar conexión con la base de datos.'
                 ]
             );
         }
         return $this->respond(['response' => $response]);
     }

     //** funcion para actualizar los sueldos parametrizados*/
    public function actualizarSueldos()
    {
        $response = null;
        $jsonSuedos = $this->request->getJsonRawBody();
        try {
           $response = $this->modelo->actualizarSueldos($jsonSuedos);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para actualizar los bonos parametrizados*/
    public function actualizarBonos()
    {
        $response = null;
        $jsonBonos = $this->request->getJsonRawBody();
        try {
           $response = $this->modelo->actualizarBonos($jsonBonos);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para actualizar el isr parametrizado*/
    public function actualizarIsr()
    {
        $response = null;
        $jsonIsr = $this->request->getJsonRawBody();

        try {
           $response = $this->modelo->actualizarIsr($jsonIsr);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para actualizar los empleados ya agregados*/
    public function actualizarEmpleados()
    {
        $response = null;
        $jsonEmpleados = $this->request->getJsonRawBody();

        try {
           $response = $this->modelo->actualizarEmpleados($jsonEmpleados);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

      //** funcion para agregar los nuevos empleados*/
    public function insertarEmpleados()
    {
        $response = null;
        $jsonEmpleados = $this->request->getJsonRawBody();

        try {
           $response = $this->modelo->insertarEmpleados($jsonEmpleados);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

    //** funcion para registrar las entregas*/
    public function insertarEntregas()
    {
        $response = null;
        $jsonEntregas = $this->request->getJsonRawBody();

        try {
           $response = $this->modelo->insertarEntregas($jsonEntregas);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }
        return $this->respond(['response' => $response]);
    }

     //** funcion para eliminar las entregas*/
     public function eliminarEntregas()
     {
         $response = null;
         $numEmpleado = $_GET['numEmpleado'];
         $fecha = $_GET['fecha'];
         try {
            $response = $this->modelo->eliminarEntregas($numEmpleado,$fecha);
         } catch (\Exception $ex) {
             $mensaje = $ex->getMessage();
             $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");
             throw new HTTPException(
                 'No fue posible completar su solicitud, intente de nuevo por favor.',
                 500, [
                     'dev' => $mensaje,
                     'internalCode' => 'SIE1000',
                     'more' => 'Verificar conexión con la base de datos.'
                 ]
             );
         }
         return $this->respond(['response' => $response]);
     }
}
