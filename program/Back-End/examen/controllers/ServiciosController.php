<?php

namespace Coppel\Rinku\Controllers;

use Coppel\RAC\Controllers\RESTController;
use Coppel\RAC\Exceptions\HTTPException;
use Coppel\Rinku\Models as Modelos;

class ServiciosController extends RESTController
{
    private $logger;
    private $modelo;

    public function onConstruct()
    {
        $this->logger = \Phalcon\DI::getDefault()->get('logger');
        $this->modelo = new Modelos\ApiModel();
    }

    public function generarPdf()
    {
        $response = null;
        $tablaRecetas = '';
        $tuplaRecetas = '';
        $tupla = '';
        $encabezado = 0;
        $html = '';
        $color = '#FFFFFF';

        try {
            $resultado = $this->modelo->consultarNominas();

            $oldLimit  = ini_get("memory_limit");
            $oldTime  = ini_get("max_execution_time");

            $numeroFilas = count($resultado);

            if($numeroFilas < 500){
                ini_set("max_execution_time", 60);
            }
            else if($numeroFilas >= 500 && $numeroFilas < 1000){
                ini_set("memory_limit", "512M");
                ini_set("max_execution_time", 180);
            }
            else if($numeroFilas >= 1000 && $numeroFilas < 2000){
                ini_set("memory_limit", "1024M");
                ini_set("max_execution_time", 240);
            }
            else if($numeroFilas >= 2000 && $numeroFilas < 3000){
                ini_set("memory_limit", "1024M");
                ini_set("max_execution_time", 300);
            }
            else if($numeroFilas >= 3000 ){
                ini_set("memory_limit", "2048M");
                ini_set("max_execution_time", 420);
            }

            $tablaRecetas = file_get_contents('./asset/templates/tablaRinku.tpl');
            $tuplaRecetas = file_get_contents('./asset/templates/tuplaRinku.tpl.txt');

            $flagColor = 0;
            $color = '#FFFFFF';
            foreach ($resultado as $value) {
                        $tupla .= str_replace(
                            ['|Empleado|', '|Nombre|','|Tipo Empleado|', '|Rol|', '|Sueldo|', '|Bonos|', '|Despensa|', '|Isr|', '|Sueldo Neto|', '|Total pagar|',
                             '|Fecha|', '|COLOR|'],
                            [$value->num_empleado, $value->nombre, $value->des_tipoempleado, $value->des_roll, $value->num_sueldo, $value->num_bonos,
                            $value->num_despensa, $value->num_isr, $value->num_sueldoNeto, $value->num_sueldoPagar,
                            $value->fecha,
                            $color ],
                            $tuplaRecetas
                        );
                if(($flagColor%2) == 0){
                    $color = '#E6E6E6';
                }
                else{
                    $color = '#FFFFFF';
                }

                $flagColor = $flagColor + 1;
            }

            $html = str_replace('|LISTADO|', $tupla, $tablaRecetas);

            $hoy = getdate();
            $nombrePdf = "Nomina_";
            $nombrePdf .= $hoy["year"].str_pad($hoy["mon"], 2, "0", STR_PAD_LEFT).str_pad($hoy["mday"], 2, "0", STR_PAD_LEFT);
            $nombrePdf .= str_pad($hoy["hours"], 2, "0", STR_PAD_LEFT).str_pad($hoy["minutes"], 2, "0", STR_PAD_LEFT).str_pad($hoy["seconds"], 2, "0", STR_PAD_LEFT);
            $nombrePdf .= ".pdf";

            $response = $this->crearPdf($html, $nombrePdf,$subtitulo);
            ini_set("memory_limit", $oldLimit);
            ini_set("max_execution_time", $oldTime);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['response' =>  $response]);
    }

    public function crearPdf($html, $nombrePdf, $subtitulo)
    {
       $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
         $filePath = __DIR__ . "/../descargas/$nombrePdf";
         $response = null;

        $pdf->setPrintHeader(true);

        $pdf->setHeaderData("/imagen/logo.png", 40, 'Nóminas Rinku');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->AddPage('L', 'A4');
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $nombre =  str_replace(" ", "_",$nombrePdf);
        $base64 = trim(strtr($pdf->Output($nombrePdf, 'E'),
                array($nombre => "", '"' => "", "name=" => "", "filename=" => "",
                    "Content-Type: application/pdf;" => "",
                    "Content-Transfer-Encoding: base64" => "",
                    "Content-Disposition: attachment;" => ""
                )
            ));
            $response['nombre'] = $nombre;
            $response['base64'] = $base64;
           // $response = $pdf->Output($nombrePdf, 'D');
        return $response;
    }
}
