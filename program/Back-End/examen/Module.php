<?php

namespace Coppel\RAC\Modules;

use PDO;
use Phalcon\Mvc\Micro\Collection;

class Module implements IModule
{
    public function __construct()
    {
    }

    public function registerLoader($loader)
    {
        $loader->registerNamespaces([
            'Coppel\Rinku\Controllers' => __DIR__ . '/controllers/',
            'Coppel\Rinku\Models' => __DIR__ . '/models/'
        ], true);
    }

    public function getCollections()
    {
        $collection = new Collection();
        $nomina = new Collection();


        $collection->setPrefix('/api')
        ->setHandler('\Coppel\Rinku\Controllers\ApiController')
        ->setLazy(true);
            $collection->get('/rinku/generar/nominas', 'generarNominas');
            $collection->get('/rinku/nominas', 'consultarNominas');
            $collection->get('/rinku/roles', 'consultarRoles');
            $collection->get('/rinku/bonos', 'consultarBonos');
            $collection->get('/rinku/sueldos', 'consultarSueldos');
            $collection->get('/rinku/parametros/bonos', 'consultarParametrosBonos');
            $collection->get('/rinku/verificar/entregas', 'consultarVerificarEntregas');
            $collection->get('/rinku/horas', 'consultarHoras');
            $collection->get('/rinku/registros/entregas', 'consultarRegistrosEntregas');
            $collection->get('/rinku/empleados', 'consultarEmpledos');
            $collection->get('/rinku/isrs', 'consultarIsr');
            $collection->get('/rinku/numempleados', 'consultarNumEmpledos');
            $collection->get('/rinku/empleados/filtros', 'consultarEmpledosFiltros');
            $collection->get('/rinku/empleados/filtros/puestos', 'consultarEmpledosFiltrosPuestos');
            $collection->get('/rinku/empleados/entregas', 'consultarEmpleadoEntregas');
            $collection->get('/rinku/entregas/flag', 'consultarFlagEntregas');
            $collection->get('/rinku/correos', 'consultarFlagCorreos');

            $collection->put('/rinku/sueldos', 'actualizarSueldos');
            $collection->put('/rinku/bonos', 'actualizarBonos');
            $collection->put('/rinku/isrs', 'actualizarIsr');
            $collection->put('/rinku/empleados', 'actualizarEmpleados');

            $collection->post('/rinku/empleados', 'insertarEmpleados');
            $collection->post('/rinku/entregas', 'insertarEntregas');

            $collection->delete('/rinku/entregas', 'eliminarEntregas');

            $servicios = new Collection();

            $servicios->setPrefix('/api')
                ->setHandler('\Coppel\Rinku\Controllers\ServiciosController')
                ->setLazy(true);

            $servicios->get('/rinku/nomina/pdf', 'generarPdf');

        return [
            $collection, $servicios
        ];
    }

    public function registerServices()
    {
        $di = \Phalcon\DI::getDefault();

        $di->set('conexion', function () use ($di) {
            $config = $di->get('config')->conexion;

            $drivers = \PDO::getAvailableDrivers();
            $cadena = "";

            if (array_search('dblib', $drivers) !== false) {
                $cadena = "dblib:host={$config->host}:{$config->port};dbname={$config->dbname};";
            } else {
                $cadena = "sqlsrv:Server={$config->host};Database={$config->dbname};";
            }
            return new PDO(
                $cadena,
                $config->username,
                $config->password, [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
                ]
            );
        });

        $di->set('logger', function () {
            return new \Katzgrau\KLogger\Logger('logs');
        });
    }
}
