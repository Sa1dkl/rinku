<?php

namespace Coppel\Rinku\Models;

use Phalcon\Mvc\Model as Modelo;

class ApiModel extends Modelo
{
   //** funcion para generar nominas */
    public function generarNominas($fecha)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_CalcularBonosRinku
        :fecha");
        $statement->bindValue('fecha', $fecha);
        $response = $statement->execute();
        return $response;
    }

      //** funcion para consultar nominas */
      public function consultarNominas()
      {
          $di = \Phalcon\DI::getDefault();
          $response = null;
          $conexion = $di->get('conexion');
          $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarNominaRinku");
          $statement->execute();
          $response = $statement->fetchAll();
          return $response;
      }

    //** funcion para consultar si existe el correo */
    public function consultarFlagCorreos($correo)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarCorreoEmpleadoRinku
        :correo");
        $statement->bindValue('correo', $correo);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar los roles */
    public function consultarRoles()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarRolesRinku");
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar los bonos */
    public function consultarBonos()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarBonosRinku");
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar los sueldos */
    public function consultarSueldos($idRoll)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarSueldosRinku
        :idRoll");
        $statement->bindValue('idRoll', $idRoll);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar los bonos para el combo*/
    public function consultarParametrosBonos($idBono)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_consultarParametrosBonosRinku
        :idBono");
        $statement->bindValue('idBono', $idBono);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para verificar si el empleado ya realizo entregas*/
    public function consultarVerificarEntregas($numEmpleado,$fecha,$numEmpleadoC)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_VerificarEntregaRinku
        :numEmpleado,
        :fecha,
        :numEmpleadoC");
        $statement->bindValue('numEmpleado', $numEmpleado);
        $statement->bindValue('fecha', $fecha);
        $statement->bindValue('numEmpleadoC', $numEmpleadoC);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar las horas de la jornada de trabajo parametrizadas*/
    public function consultarHoras($idRoll)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarHorasRinku
        :idRoll");
        $statement->bindValue('idRoll', $idRoll);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar las entregas realizadas por empelado*/
    public function consultarRegistrosEntregas($numEmpleado,$fecha)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarEntregasRinku
        :numEmpleado,
        :fecha");
        $statement->bindValue('numEmpleado', $numEmpleado);
        $statement->bindValue('fecha', $fecha);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar los empleados registrados*/
    public function consultarEmpledos()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_consultarEmpleadosRinku
        ");
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar el isr parametrizado*/
    public function consultarIsr($validar)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarIsrRinku
        :validar");
        $statement->bindValue('validar', $validar);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para consultar el proximo numero de empleado disponible a insertar*/
    public function consultarNumEmpledos()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarNumEmpleadoRinku");
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para ver el filtro de empleados ya agregados*/
    public function consultarEmpledosFiltros()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarEmpledosFiltrosRinku");
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

     //** funcion para ver el filtro de empleados ya agregados sin auxiliares*/
    public function consultarEmpledosFiltrosPuestos()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarEmpledosFiltrosPuestosRinku");
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

    //** funcion para optener la informacion del empleado*/
    public function consultarEmpleadoEntregas($numEmpleado)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ConsultarEmpleadosEntregasRinku
        :numEmpleado");
        $statement->bindValue('numEmpleado', $numEmpleado);
        $statement->execute();
        $response = $statement->fetchAll();
        return $response;
    }

     //** funcion para ver si el empleado puede cubrir*/
     public function consultarFlagEntregas($fecha)
     {
         $di = \Phalcon\DI::getDefault();
         $response = null;
         $conexion = $di->get('conexion');
         $statement = $conexion->prepare("EXEC examen.dbo.proc_consultarFlagEntregasRinku
         :fecha");
         $statement->bindValue('fecha', $fecha);
         $statement->execute();
         $response = $statement->fetchAll();
         return $response;
     }

    //** funcion para actualizar los sueldos parametrizados*/
    public function actualizarSueldos($jsonSuedos)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ActualizarSueldosRinku
        :idRoll,
        :sueldo,
        :horas,
        :porcentaje");
        $statement->bindValue('idRoll', $jsonSuedos->idRoll);
        $statement->bindValue('sueldo', $jsonSuedos->sueldo);
        $statement->bindValue('horas', $jsonSuedos->horas);
        $statement->bindValue('porcentaje', $jsonSuedos->porcentaje);
        $response = $statement->execute();
        return $response;
    }

    //** funcion para actualizar los bonos parametrizados*/
    public function actualizarBonos($jsonBonos)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ActualizarBonosRinku
        :idBono,
        :bono");
        $statement->bindValue('idBono', $jsonBonos->idBono);
        $statement->bindValue('bono', $jsonBonos->bono);
        $response = $statement->execute();
        return $response;
    }

    //** funcion para actualizar el isr parametrizado*/
    public function actualizarIsr($jsonIsr)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ActualizarIsrRinku
        :validar,
        :valor,
        :isr");
        $statement->bindValue('validar',  $jsonIsr->validar);
        $statement->bindValue('valor',  $jsonIsr->valor);
        $statement->bindValue('isr',  $jsonIsr->isr);
        $response = $statement->execute();
        return $response;
    }

    //** funcion para actualizar los empleados ya agregados*/
    public function actualizarEmpleados($jsonEmpleados)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_ActualizarEmpleadosRinku
        :numEmpleado,
        :nombre,
        :apellidoP,
        :apellidoM,
        :correo,
        :tipo,
        :roll,
        :flag");
        $statement->bindValue('numEmpleado', $jsonEmpleados->numEmpleado);
        $statement->bindValue('nombre', $jsonEmpleados->nombre);
        $statement->bindValue('apellidoP', $jsonEmpleados->apellidoP);
        $statement->bindValue('apellidoM', $jsonEmpleados->apellidoM);
        $statement->bindValue('correo', $jsonEmpleados->correo);
        $statement->bindValue('tipo', $jsonEmpleados->tipo);
        $statement->bindValue('roll', $jsonEmpleados->roll);
        $statement->bindValue('flag', $jsonEmpleados->flag);
        $response = $statement->execute();
        return $response;
    }

    //** funcion para agregar los nuevos empleados*/
    public function insertarEmpleados($jsonEmpleados)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_AltaEmpleadosRinku
        :numEmpleado,
        :nombre,
        :apellidoP,
        :apellidoM,
        :correo,
        :tipo,
        :roll");
        $statement->bindValue('numEmpleado', $jsonEmpleados->numEmpleado);
        $statement->bindValue('nombre', $jsonEmpleados->nombre);
        $statement->bindValue('apellidoP', $jsonEmpleados->apellidoP);
        $statement->bindValue('apellidoM', $jsonEmpleados->apellidoM);
        $statement->bindValue('correo', $jsonEmpleados->correo);
        $statement->bindValue('tipo', $jsonEmpleados->tipo);
        $statement->bindValue('roll', $jsonEmpleados->roll);
        $response = $statement->execute();
        return $response;
    }

    //** funcion para registrar las entregas*/
    public function insertarEntregas($jsonEntregas)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_GuardarEntregasRinku
        :numEmpleado,
        :nomEmpleado,
        :fecha,
        :numEntregas,
        :horas,
        :numEmpleadoCubrio,
        :nomEmpleadoCubrio,
        :flag");
        $statement->bindValue('numEmpleado', $jsonEntregas->numEmpleado);
        $statement->bindValue('nomEmpleado', $jsonEntregas->nomEmpleado);
        $statement->bindValue('fecha', $jsonEntregas->fecha);
        $statement->bindValue('numEntregas', $jsonEntregas->numEntregas);
        $statement->bindValue('horas', $jsonEntregas->horas);
        $statement->bindValue('numEmpleadoCubrio', $jsonEntregas->numEmpleadoCubrio);
        $statement->bindValue('nomEmpleadoCubrio', $jsonEntregas->nomEmpleadoCubrio);
        $statement->bindValue('flag', $jsonEntregas->flag);
        $response = $statement->execute();
        return $response;

    }

    //** funcion para eliminar las entregas*/
    public function eliminarEntregas($numEmpleado,$fecha)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $conexion = $di->get('conexion');
        $statement = $conexion->prepare("EXEC examen.dbo.proc_EliminarEntregasRinku
        :numEmpleado,
        :fecha");
        $statement->bindValue('numEmpleado', $numEmpleado);
        $statement->bindValue('fecha', $fecha);
        $response =  $statement->execute();
        return $response;
    }
}
