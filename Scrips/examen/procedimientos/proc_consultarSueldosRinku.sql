USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarSueldosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarSueldosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarSueldosRinku]
	@id_roll [INT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
		SELECT num_sueldo_hora,num_horas,prc_despensa 
		FROM [examen].[dbo].[Cat_SueldosRinku] 
		WHERE id_roll = @id_roll
		
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
