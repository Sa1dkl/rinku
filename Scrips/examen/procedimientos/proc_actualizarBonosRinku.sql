USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ActualizarBonosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ActualizarBonosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ActualizarBonosRinku]
	@id_bono [INT],
	@bono [FLOAT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
		UPDATE [examen].[dbo].[Cat_BonosRolesRinku] 
		SET num_bono=@bono  
		WHERE id_bono = @id_bono
		
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
