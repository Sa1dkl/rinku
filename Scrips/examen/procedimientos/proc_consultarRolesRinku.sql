USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarRolesRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarRolesRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarRolesRinku]
WITH
EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT id_roll,CAST(id_roll AS [VARCHAR])+'-'+des_roll AS roll 
		FROM  [examen].dbo.[Cat_RolesRinku]

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END