USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO--
IF OBJECT_ID('[dbo].[proc_VerificarEntregaRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_VerificarEntregaRinku]
END
GO--proc_VerificarEntregaRinku '00000001','20190311',''
CREATE PROCEDURE [dbo].[proc_VerificarEntregaRinku]
	@num_empleado [CHAR](8),
	@fec_movimiento	[VARCHAR](10),
	@num_empleadoC [CHAR](8)
	

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		IF EXISTS (SELECT * FROM Cat_MovimientosEmpeladoRinku WHERE num_empleado = @num_empleado AND fec_movimiento = @fec_movimiento)  
			BEGIN
				SELECT 1 AS flagExiste
			END
		ELSE
		BEGIN 
		IF EXISTS (SELECT TOP 1 1 FROM Cat_MovimientosEmpeladoRinku WHERE num_empleado = @num_empleadoC AND fec_movimiento = @fec_movimiento )
			BEGIN
				SELECT 2 AS flagExiste
			END
		ELSE
			BEGIN
			IF EXISTS (SELECT *FROM Cat_MovimientosEmpeladoRinku WHERE num_empleadocubrio =  @num_empleado AND fec_movimiento = @fec_movimiento )
			BEGIN
				SELECT 1 AS flagExiste
			END
			ELSE
			BEGIN 
			SELECT 0 AS flagExiste
			END
			END
		END

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

