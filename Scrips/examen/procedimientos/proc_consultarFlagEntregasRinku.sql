USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_consultarFlagEntregasRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_consultarFlagEntregasRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_consultarFlagEntregasRinku]
	@fec_movimiento	[VARCHAR](10)
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @fechaIni [VARCHAR](10), @fechaFin [VARCHAR](10)

	SET @fechaIni =(CAST (LEFT(REPLACE(@fec_movimiento,'-',''),6)+'01' AS [DATE]))
	SET @fechaFin =(SELECT DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(REPLACE(@fec_movimiento,'-',''),6)+'01' AS [DATE]))))

		IF EXISTS (SELECT TOP 1 1 FROM Cat_MovimientosEmpeladoRinku WHERE fec_movimiento >= @fechaIni AND fec_movimiento <  @fechaFin)
		BEGIN
			SELECT 1 AS flag
		END
		ELSE
		BEGIN
			SELECT 0 AS flag
		END

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

