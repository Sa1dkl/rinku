USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarEmpleadosEntregasRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarEmpleadosEntregasRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_consultarEmpleadosEntregasRinku]  
	@num_empleado [VARCHAR](10)  
WITH  
 EXECUTE AS OWNER  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
	 SELECT nom_empleado+' '+ape_paterno+' '+ape_materno AS nombre,des_roll,des_tipoempleado,flag_cubrepuesto 
	 FROM  [examen].[dbo].[Cat_EmpleadosRinku] AS a   
	 INNER JOIN  [examen].[dbo].[Cat_EmpleadosRolesRinku] AS b ON a.num_empleado = b.num_empleado   
	 INNER JOIN  [examen].[dbo].[Cat_RolesRinku] AS c ON b.id_roll = c.id_roll  
	 INNER JOIN  [examen].[dbo].[cat_TipoEmpleadoRinku] AS d ON a.tipo_empleado = d.id_tipoempleado  
	 WHERE a.num_empleado = @num_empleado  AND a.flag_activo = 1
  
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED;  
 SET NOCOUNT OFF;  
END  
  