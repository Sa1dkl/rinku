USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarEntregasRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarEntregasRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarEntregasRinku]
	@num_empleado [CHAR](8),
	@fec_movimiento	[VARCHAR](10)
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	DECLARE @nombre [VARCHAR](50), @numEmpleado [CHAR](8)
	DECLARE @entregas [INT], @horas [INT], @id_bono [INT], @fechaIni [VARCHAR](10), @fechaFin [VARCHAR](10), @bonoEntrega [INT]

	SET @fechaIni =(CAST (LEFT(REPLACE(@fec_movimiento,'-',''),6)+'01' AS [DATE]))
	SET @fechaFin =(SELECT DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(REPLACE(@fec_movimiento,'-',''),6)+'01' AS [DATE]))))

		SELECT a.num_empleado,a.nom_empleado,num_entregas,num_horas,a.num_empleadocubrio,a.nom_empleadocubrio,
		a.id_bono,CAST(a.fec_movimiento AS [DATE]) AS fec_movimiento ,flag_cubrio 
		INTO #Cat_MovimientosEmpeladoRinku 
		FROM [examen].[dbo].[Cat_MovimientosEmpeladoRinku] AS a 
		INNER JOIN [examen].[dbo].[Cat_EmpleadosRinku] AS b  ON a.num_empleado = b.num_empleado
		INNER JOIN [examen].[dbo].[Cat_BonosRolesRinku] AS c ON a.id_bono = c.id_bono
		WHERE a.num_empleado = @num_empleado AND a.fec_movimiento >= @fechaIni AND a.fec_movimiento < @fechaFin AND b.flag_activo = 1
		
		SELECT * FROM #Cat_MovimientosEmpeladoRinku
	
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

