USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_CalcularSueldoRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_CalcularSueldoRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_CalcularSueldoRinku]
	@fecha [VARCHAR](10)

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @entregas [INT], @horas [INT], @id_bono [INT], @fechaIni [VARCHAR](10), @fechaFin [VARCHAR](10), @bonoEntrega [INT]

	SET @fechaIni =(CAST (LEFT(REPLACE(@fecha,'-',''),6)+'01' AS [DATE]))
	SET @fechaFin =(SELECT DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(REPLACE(@fecha,'-',''),6)+'01' AS [DATE]))))
	SET @bonoEntrega = (SELECT num_bono FROM Cat_BonosRolesRinku WHERE id_roll = 0)

	CREATE TABLE #pasoSueldo
	(
		num_empleado [CHAR](8),
		num_sueldo [INT],
		fecha [DATE],
		tipo [INT]
	)

	INSERT INTO #pasoSueldo
	SELECT a.num_empleado,SUM(a.num_horas)*num_sueldo_hora AS num_sueldo,fec_movimiento,c.tipo_empleado 
	FROM [examen].[dbo].[Cat_MovimientosEmpeladoRinku] AS a
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRolesRinku] AS d ON a.num_empleado = d.num_empleado
	INNER JOIN [examen].[dbo].[Cat_SueldosRinku] AS b ON d.id_roll = b.id_roll
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRinku] AS c ON a.num_empleado = c.num_empleado
	WHERE CAST(fec_movimiento AS DATE) >= @fechaIni AND CAST(fec_movimiento AS DATE) <= @fechaFin AND c.flag_activo = 1
	GROUP BY a.num_empleado,fec_movimiento,num_sueldo_hora,tipo_empleado

	TRUNCATE TABLE [examen].[dbo].[cat_SueldosNetosRinku]
	
	SELECT a.num_Empleado,SUM(num_sueldo) AS num_sueldo,b.num_bonos,0 AS despensa,a.tipo 
	INTO #pasotodo 
	FROM #pasoSueldo AS a
	INNER JOIN examen.dbo.cat_bonosnetos AS b ON b.num_empleado = a.num_empleado
	GROUP BY a.num_empleado,num_bonos,tipo

	SELECT a.num_empleado,a.num_sueldo,a.num_bonos,(a.num_sueldo * CAST(d.prc_despensa AS [FLOAT]) /100)AS num_despensa,tipo 
	INTO #pasosueldoactualizado 
	FROM #pasotodo AS a 
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRolesRinku] AS b ON  a.num_empleado  = b.num_empleado
	INNER JOIN [examen].[dbo].[Cat_SueldosRinku] AS d ON d.id_roll = b.id_roll
	
	UPDATE #pasosueldoactualizado SET num_despensa = 0 WHERE tipo = 2
	

	SELECT num_empleado,num_sueldo,num_bonos,num_despensa,(num_sueldo+num_bonos+num_despensa) AS num_sueldoNeto,0 AS isr 
	INTO #pasofinal 
	FROM #pasosueldoactualizado
	
	SELECT * , 0 flag_isr, a.num_sueldoneto*(b.prc_isr/100.0) num_cantidad 
	INTO #tmpisr
	FROM #pasofinal a ,Cat_isrRinku b


	UPDATE
	#tmpisr
	SET flag_isr=
	CASE WHEN num_sueldoNeto <= num_isr THEN 1
	WHEN num_sueldoNeto > num_isr THEN 2
	ELSE 0
	END

	DELETE FROM 
	#tmpisr
	WHERE id_isr!=flag_isr
	
	INSERT INTO cat_sueldosnetosrinku
	SELECT num_empleado, ROUND(CAST (num_sueldo AS [DECIMAL](6,2)),1)AS num_sueldo,ROUND(CAST (num_bonos AS [DECIMAL](6,2)),1)AS num_bonos,
	ROUND(CAST (num_despensa AS [DECIMAL](6,2)),1) AS despensa, ROUND(CAST (num_cantidad AS [DECIMAL](6,2)),1)AS num_cantidad,
	ROUND(CAST (num_sueldoNeto AS [DECIMAL](6,2)),1) AS num_sueldoNeto,ROUND(CAST (num_sueldoNeto - num_cantidad AS [DECIMAL](6,2)),1) AS num_sueldoneto 
	FROM #tmpisr
	
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
