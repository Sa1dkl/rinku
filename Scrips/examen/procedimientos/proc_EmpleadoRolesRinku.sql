USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_EmpleadoRolesRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_EmpleadoRolesRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_EmpleadoRolesRinku]
	@num_empleado [CHAR](8),
	@id_roll	[INT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF EXISTS(SELECT TOP 1 1 FROM  Cat_EmpleadosRolesRinku WHERE num_empleado = @num_empleado )
		BEGIN
			UPDATE Cat_EmpleadosRolesRinku SET id_roll = @id_roll WHERE num_empleado = @num_empleado
		END
	ELSE
		BEGIN
			INSERT INTO Cat_EmpleadosRolesRinku VALUES (@num_empleado,@id_roll)
		END

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
