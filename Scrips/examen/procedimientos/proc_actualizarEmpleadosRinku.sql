USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ActualizarEmpleadosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ActualizarEmpleadosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ActualizarEmpleadosRinku]
	@num_empleado [VARCHAR](10),
	@nom_empleado	[VARCHAR](30),
	@ape_paterno	[VARCHAR](30),
	@ape_materno	[VARCHAR](30),
	@nom_correo	[VARCHAR](50),
	@tipo_empleado	[TINYINT],
	@id_roll	[TINYINT],
	@flag [TINYINT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE [examen].[dbo].[Cat_EmpleadosRinku] 
	SET nom_empleado =@nom_empleado,ape_paterno=@ape_paterno,ape_materno=@ape_materno,
	nom_correo = @nom_correo,tipo_empleado=@tipo_empleado,flag_activo=@flag 
	WHERE num_empleado = @num_empleado
	
	EXEC proc_EmpleadoRolesRinku  @num_empleado,@id_roll

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END