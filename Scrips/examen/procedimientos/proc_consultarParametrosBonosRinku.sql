USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_consultarParametrosBonosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_consultarParametrosBonosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_consultarParametrosBonosRinku]
	@id_bono [INT]
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT CAST(id_bono AS [VARCHAR])+'-'+des_bono AS name,des_roll,num_bono,id_bono 
	INTO #pasoBono 
	FROM [examen].[dbo].[Cat_BonosRolesRinku] AS a 
	INNER JOIN [examen].[dbo].[Cat_RolesRinku] AS b ON a.id_roll = b.id_roll 
	WHERE id_bono = @id_bono
	
	UPDATE #pasoBono SET des_roll = 'TODOS' WHERE id_bono = 4
	
	SELECT * FROM #pasoBono

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

