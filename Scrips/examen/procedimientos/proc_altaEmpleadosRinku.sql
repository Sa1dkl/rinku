USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_AltaEmpleadosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_AltaEmpleadosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_AltaEmpleadosRinku]
	@num_empleado [VARCHAR](10),
	@nom_empleado	[VARCHAR](30),
	@ape_paterno	[VARCHAR](30),
	@ape_materno	[VARCHAR](30),
	@nom_correo	[VARCHAR](50),
	@tipo_empleado	[TINYINT],
	@id_roll	[TINYINT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO Cat_EmpleadosRinku VALUES (@num_empleado,@nom_empleado,@ape_paterno,@ape_materno,@nom_correo,@tipo_empleado,GETDATE(),1)
	
	EXEC proc_EmpleadoRolesRinku  @num_empleado,@id_roll

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END