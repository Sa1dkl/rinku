USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ActualizarSueldosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ActualizarSueldosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ActualizarSueldosRinku]
	@id_roll [INT],
	@sueldo [FLOAT],
	@horas [INT],
	@porcentaje [FLOAT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
		UPDATE [examen].[dbo].[Cat_SueldosRinku] 
		SET num_sueldo_hora=@sueldo,num_horas= @horas,prc_despensa=@porcentaje 
		WHERE id_roll = @id_roll
		
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
