USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarEmpledosFiltrosPuestosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarEmpledosFiltrosPuestosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarEmpledosFiltrosPuestosRinku]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
		SELECT RIGHT('0000000' + LTRIM(RTRIM(a.num_empleado)),8) AS numEmpleado 
		FROM  [examen].[dbo].[Cat_EmpleadosRinku] AS a
		INNER JOIN [examen].[dbo].[Cat_EmpleadosRolesRinku] AS b ON a.num_empleado=b.num_empleado 
		WHERE b.id_roll != 3 AND a.flag_activo = 1

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END