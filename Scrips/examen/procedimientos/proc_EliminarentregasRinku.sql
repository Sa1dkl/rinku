USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_EliminarEntregasRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_EliminarEntregasRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_EliminarEntregasRinku]
	@num_empleado [CHAR](8),
	@fecha [VARCHAR](10)

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		DELETE Cat_MovimientosEmpeladoRinku 
		WHERE num_empleado = @num_empleado AND fec_movimiento = @fecha

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

