USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_CalcularBonosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_CalcularBonosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_CalcularBonosRinku]
	@fecha [VARCHAR](10)
	

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @entregas [INT], @horas [INT], @id_bono [INT], @fechaIni [VARCHAR](10), @fechaFin [VARCHAR](10), @bonoEntrega [INT]

	SET @fechaIni =(CAST (LEFT(REPLACE(@fecha,'-',''),6)+'01' AS [DATE]))
	SET @fechaFin =(SELECT DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(REPLACE(@fecha,'-',''),6)+'01' AS [DATE]))))
	SET @bonoEntrega = (SELECT num_bono FROM [examen].[dbo].[Cat_BonosRolesRinku] WHERE id_bono = 4)

	CREATE TABLE #paso
	(
		num_empleado [CHAR](8),
		horas [INT],
		entregas [INT],
		fecha [DATE]
	)

	INSERT INTO #paso
	SELECT a.num_empleado,SUM(num_horas) * b.num_bono,SUM(num_entregas)*@bonoEntrega,fec_movimiento   
	FROM [examen].[dbo].[Cat_MovimientosEmpeladoRinku] AS a
	INNER JOIN [examen].[dbo].[Cat_BonosRolesRinku] AS b ON a.id_bono = b.id_bono
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRolesRinku] AS d ON a.num_empleado = d.num_empleado
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRinku] AS c ON a.num_empleado = c.num_empleado
	WHERE CAST(fec_movimiento AS [DATE]) >=@fechaIni AND CAST(fec_movimiento AS [DATE]) <= @fechaFin AND c.flag_activo = 1
	GROUP BY a.num_empleado,fec_movimiento,num_bono

	TRUNCATE TABLE [examen].[dbo].[Cat_BonosNetos]
	INSERT INTO [examen].[dbo].[Cat_BonosNetos]
	SELECT num_empleado,SUM(entregas) + SUM(horas) FROM #paso GROUP BY num_empleado
	
	EXEC proc_CalcularSueldoRinku @fecha
	
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
