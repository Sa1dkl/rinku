USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarNominaRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarNominaRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarNominaRinku]
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT a.num_empleado,a.num_sueldo,a.num_bonos,a.num_despensa,a.num_isr,a.num_sueldoNeto,a.num_sueldoPagar,
	b.nom_empleado+' '+b.ape_paterno+' '+b.ape_materno AS nombre,e.des_tipoempleado,c.des_roll,CAST (GETDATE() AS [DATE])  AS fecha 
	FROM [examen].[dbo].[cat_sueldosnetosrinku] AS a
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRinku] AS b ON a.num_empleado = b.num_empleado
	INNER JOIN [examen].[dbo].[Cat_EmpleadosRolesRinku] AS d ON a.num_empleado = d.num_empleado
	INNER JOIN [examen].[dbo].[Cat_RolesRinku] AS c ON c.id_roll = d.id_roll
	INNER JOIN [examen].[dbo].[cat_TipoEmpleadoRinku] AS e ON e.id_tipoempleado = b.tipo_empleado

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

