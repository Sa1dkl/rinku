USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarBonosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarBonosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarBonosRinku]
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT des_bono+'-'+b.des_roll AS des_bono,id_bono 
		INTO #paso FROM Cat_BonosRolesRinku  AS a
		INNER JOIN cat_rolesrinku AS b ON a.id_roll = b.id_roll
		
		UPDATE #paso SET des_bono = 'Por Entrega Todos' WHERE id_bono = 4
		
		SELECT * FROM #paso
	
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

