USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_consultarEmpleadosRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_consultarEmpleadosRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_consultarEmpleadosRinku]
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT a.num_empleado,nom_empleado,ape_paterno,ape_materno,nom_correo,tipo_empleado,fec_alta,flag_activo,b.id_roll 
		FROM [examen].[dbo].[Cat_EmpleadosRinku] AS a 
		INNER JOIN Cat_EmpleadosRolesRinku AS b ON a.num_empleado = b.num_empleado 

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END

