USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ConsultarHorasRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ConsultarHorasRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ConsultarHorasRinku]
	@num_empleado [VARCHAR](10)

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @id_roll [INT]
	
	SET @id_roll = (SELECT  id_roll FROM [examen].[dbo].[Cat_EmpleadosRolesRinku] WHERE num_empleado = @num_empleado)

	SELECT num_horas 
	FROM [examen].[dbo].[Cat_SueldosRinku] 
	WHERE id_roll = @id_roll

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END