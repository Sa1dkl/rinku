USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_GuardarEntregasRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_GuardarEntregasRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_GuardarEntregasRinku]
	@num_empleado [CHAR](8),
	@nombre [VARCHAR](50),
	@fec_movimiento	[VARCHAR](10),
	@num_entregas	[INT],
	@horas [INT],
	@numEmpeladoCubrio [CHAR](8),
	@nombreCubrio [VARCHAR](50),
	@flag [TINYINT]
WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @id_bono [TINYINT],@id_roll [TINYINT]
	
	IF (@flag = 0)
		BEGIN
			SET @id_roll =(SELECT id_roll FROM [examen].[dbo].[Cat_EmpleadosRolesRinku] WHERE num_empleado = @num_empleado)
			SET @id_bono = (SELECT id_bono FROM [examen].[dbo].[Cat_BonosRolesRinku] WHERE id_roll = @id_roll AND flag_bono = 1)
		END
	ELSE
		BEGIN
			SET @id_roll =(SELECT id_roll FROM [examen].[dbo].[Cat_EmpleadosRolesRinku] WHERE num_empleado = @numEmpeladoCubrio)
			SET @id_bono = (SELECT id_bono FROM [examen].[dbo].[Cat_BonosRolesRinku] WHERE id_roll = @id_roll AND flag_bono = 1)
		END

	INSERT INTO Cat_MovimientosEmpeladoRinku 
	VALUES (@num_empleado,@nombre,@fec_movimiento,@num_entregas,@horas,@numEmpeladoCubrio,@nombreCubrio,0,0,@id_bono,@flag)

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
