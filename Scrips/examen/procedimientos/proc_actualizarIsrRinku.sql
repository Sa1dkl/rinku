USE [examen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[proc_ActualizarIsrRinku]','P') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[proc_ActualizarIsrRinku]
END
GO
CREATE PROCEDURE [dbo].[proc_ActualizarIsrRinku]
	@validar [INT],
	@valor [INT],
	@porcentaje [INT]

WITH
	EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
		UPDATE [examen].[dbo].[Cat_isrRinku] 
		SET num_isr=@valor,prc_isr=@porcentaje 
		WHERE id_isr = @validar
		
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT OFF;
END
