IF OBJECT_ID('[examen].[dbo].[Cat_EmpleadosRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_EmpleadosRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_EmpleadosRinku]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
		nom_empleado [VARCHAR](30),
		ape_paterno	 [VARCHAR](30),
		ape_materno	 [VARCHAR](30),
		nom_correo	[VARCHAR](50),
		tipo_empleado [TINYINT],
		fec_alta [DATETIME],
		flag_activo [BIT],
		FOREIGN KEY (tipo_empleado) REFERENCES [examen].[dbo].[Cat_TipoEmpleadoRinku](id_tipoempleado)
	)
END