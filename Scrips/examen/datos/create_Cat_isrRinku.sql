IF OBJECT_ID('[examen].[dbo].[Cat_isrRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_isrRinku]
END
BEGIN
CREATE TABLE [examen].[dbo].[Cat_isrRinku]
	(
        id_isr [INT] PRIMARY KEY,
		des_isr  [VARCHAR](50),
		num_isr [INT],
		prc_isr [INT]
	)
END