IF OBJECT_ID('[examen].[dbo].[Cat_BonosRolesRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_BonosRolesRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_BonosRolesRinku]
	(
        id_roll [INT],
		num_bono [INT],
		id_bono [INT] PRIMARY KEY  ,
		des_bono[VARCHAR](50),
		flag_bono [INT]
	) 
END