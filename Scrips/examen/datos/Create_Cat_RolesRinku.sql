IF OBJECT_ID('[examen].[dbo].[Cat_RolesRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_RolesRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_RolesRinku]
	(
        id_roll [INT] PRIMARY KEY,
		des_roll [VARCHAR](50),
		flag_cubrepuesto [BIT]
	)
END