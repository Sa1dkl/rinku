IF OBJECT_ID('[examen].[dbo].[Cat_TipoEmpleadoRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[cat_TipoEmpleadoRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[cat_TipoEmpleadoRinku]
	(
        id_tipoempleado [TINYINT] PRIMARY KEY,
		des_tipoempleado [VARCHAR](30),
	)
END