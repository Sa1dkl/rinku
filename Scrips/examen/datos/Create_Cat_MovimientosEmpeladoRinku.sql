IF OBJECT_ID('[examen].[dbo].[Cat_MovimientosEmpeladoRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_MovimientosEmpeladoRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_MovimientosEmpeladoRinku]
	(
        num_empleado [CHAR](8),
		nom_empleado [VARCHAR](50),
		fec_movimiento [DATETIME],
		num_entregas [INT],
		num_horas [INT],
		num_empleadocubrio [VARCHAR](10),
		nom_empleadocubrio [VARCHAR](50),
		num_entregasC [INT],
		num_horasC [INT],
		id_bono [INT],
		flag_cubrio [INT],
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado),
		FOREIGN KEY (id_bono) REFERENCES [examen].[dbo].[Cat_BonosRolesRinku](id_bono)
	) 
END