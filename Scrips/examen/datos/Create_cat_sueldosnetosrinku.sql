IF OBJECT_ID('[examen].[dbo].[cat_SueldosNetosRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[cat_SueldosNetosRinku]
END
BEGIN
		CREATE TABLE [examen].[dbo].[cat_SueldosNetosRinku]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
        num_sueldo [FLOAT],
		num_bonos [FLOAT],
		num_despensa [DECIMAL](6,2),
		num_isr [DECIMAL](6,2),
		num_sueldoNeto [DECIMAL](6,2),
		num_sueldoPagar [DECIMAL](6,2)
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado)
	)
END