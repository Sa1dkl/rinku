IF OBJECT_ID('[examen].[dbo].[Cat_BonosNetos]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_BonosNetos]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_BonosNetos]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
		num_bonos [FLOAT]
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado)
	)
END