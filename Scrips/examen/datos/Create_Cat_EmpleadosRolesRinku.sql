IF OBJECT_ID('[examen].[dbo].[Cat_EmpleadosRolesRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_EmpleadosRolesRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_EmpleadosRolesRinku]
	(
        num_empleado [char](8) PRIMARY KEY,
		id_roll [INT],
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado),
		FOREIGN KEY (id_roll) REFERENCES [examen].[dbo].[cat_rolesRinku](id_roll) 
	) 
END