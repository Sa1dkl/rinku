IF OBJECT_ID('[examen].[dbo].[Cat_SueldosRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_SueldosRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_SueldosRinku]
	(
        id_roll [INT] PRIMARY KEY,
		num_sueldo_hora [INT],
		num_horas [INT],
		prc_despensa [INT]
		FOREIGN KEY (id_roll) REFERENCES [examen].[dbo].[cat_rolesRinku](id_roll) 
	)
END