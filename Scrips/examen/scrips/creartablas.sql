IF OBJECT_ID('[examen].[dbo].[Cat_TipoEmpleadoRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[cat_TipoEmpleadoRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[cat_TipoEmpleadoRinku]
	(
        id_tipoempleado [TINYINT] PRIMARY KEY,
		des_tipoempleado [VARCHAR](30),
	)
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_EmpleadosRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_EmpleadosRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_EmpleadosRinku]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
		nom_empleado [VARCHAR](30),
		ape_paterno	 [VARCHAR](30),
		ape_materno	 [VARCHAR](30),
		nom_correo	[VARCHAR](50),
		tipo_empleado [TINYINT],
		fec_alta [DATETIME],
		flag_activo [BIT],
		FOREIGN KEY (tipo_empleado) REFERENCES [examen].[dbo].[Cat_TipoEmpleadoRinku](id_tipoempleado)
	)
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_RolesRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_RolesRinku]
END

BEGIN

	CREATE TABLE [examen].[dbo].[Cat_RolesRinku]
	(
        id_roll [INT] PRIMARY KEY,
		des_roll [VARCHAR](50),
		flag_cubrepuesto [BIT]
	)
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_EmpleadosRolesRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_EmpleadosRolesRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_EmpleadosRolesRinku]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
		id_roll [INT],
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado),
		FOREIGN KEY (id_roll) REFERENCES [examen].[dbo].[cat_rolesRinku](id_roll) 
	) 
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_BonosRolesRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_BonosRolesRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_BonosRolesRinku]
	(
        id_roll [INT],
		num_bono [INT],
		id_bono [INT] PRIMARY KEY  ,
		des_bono[VARCHAR](50),
		flag_bono [INT]
	) 
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_MovimientosEmpeladoRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_MovimientosEmpeladoRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_MovimientosEmpeladoRinku]
	(
        num_empleado [CHAR](8),
		nom_empleado [VARCHAR](50),
		fec_movimiento [DATETIME],
		num_entregas [INT],
		num_horas [INT],
		num_empleadocubrio [VARCHAR](10),
		nom_empleadocubrio [VARCHAR](50),
		num_entregasC [INT],
		num_horasC [INT],
		id_bono [INT],
		flag_cubrio [INT],
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado),
		FOREIGN KEY (id_bono) REFERENCES [examen].[dbo].[Cat_BonosRolesRinku](id_bono)
	) 
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_isrRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_isrRinku]
END
BEGIN
CREATE TABLE [examen].[dbo].[Cat_isrRinku]
	(
        id_isr [INT] PRIMARY KEY,
		des_isr  [VARCHAR](50),
		num_isr [INT],
		prc_isr [INT]
	)
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_BonosNetos]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[Cat_BonosNetos]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_BonosNetos]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
		num_bonos [FLOAT]
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado)
	)
END
GO
IF OBJECT_ID('[examen].[dbo].[Cat_SueldosRinku]','U') IS NOT NULL
BEGIN
DROP TABLE [examen].[dbo].[Cat_SueldosRinku]
END
BEGIN
	CREATE TABLE [examen].[dbo].[Cat_SueldosRinku]
	(
        id_roll [INT] PRIMARY KEY,
		num_sueldo_hora [INT],
		num_horas [INT],
		prc_despensa [INT]
		FOREIGN KEY (id_roll) REFERENCES cat_rolesRinku(id_roll) 
	)
END
GO
IF OBJECT_ID('[examen].[dbo].[cat_SueldosNetosRinku]','U') IS NOT NULL
	BEGIN
	DROP TABLE [examen].[dbo].[cat_SueldosNetosRinku]
END
BEGIN
		CREATE TABLE [examen].[dbo].[cat_SueldosNetosRinku]
	(
        num_empleado [CHAR](8) PRIMARY KEY,
        num_sueldo [FLOAT],
		num_bonos [FLOAT],
		num_despensa [DECIMAL](6,2),
		num_isr [DECIMAL](6,2),
		num_sueldoNeto [DECIMAL](6,2),
		num_sueldoPagar [DECIMAL](6,2)
		FOREIGN KEY (num_empleado) REFERENCES [examen].[dbo].[Cat_EmpleadosRinku](num_empleado)
	)
END
	
INSERT INTO [examen].[dbo].[Cat_isrRinku] VALUES (1,'<=',16000,9)
INSERT INTO [examen].[dbo].[Cat_isrRinku] VALUES (2,'>',16000,12)
INSERT INTO [examen].[dbo].[Cat_RolesRinku] VALUES (1,'Chofer',0)
INSERT INTO [examen].[dbo].[Cat_RolesRinku] VALUES (2,'Cargador',0)
INSERT INTO [examen].[dbo].[Cat_RolesRinku] VALUES (3,'Auxiliar',1)
INSERT INTO [examen].[dbo].[Cat_TipoEmpleadoRinku] VALUES( 1,'Interno')
INSERT INTO [examen].[dbo].[Cat_TipoEmpleadoRinku] VALUES( 2,'Externo')
INSERT INTO [examen].[dbo].[Cat_SueldosRinku] VALUES (1,30,8,4)
INSERT INTO [examen].[dbo].[Cat_SueldosRinku] VALUES (2,30,8,4)
INSERT INTO [examen].[dbo].[Cat_SueldosRinku] VALUES (3,30,8,4)
INSERT INTO [examen].[dbo].[Cat_BonosRolesRinku] VALUES(1,10,1,'Por Hora',1)
INSERT INTO [examen].[dbo].[Cat_BonosRolesRinku] VALUES(2,5,2,'Por Hora',1)
INSERT INTO [examen].[dbo].[Cat_BonosRolesRinku] VALUES(3,0,3,'Por Hora',1)
INSERT INTO [examen].[dbo].[Cat_BonosRolesRinku] VALUES(1,5,4,'Por Entregas',2)